# Installation
While mytoken is still under active development, we provide packaged versions for different operating systems and distributions.

You can download a packaged release version for your operating system from the [release page](https://github.com/oidc-mytoken/client/releases).
Packages (for many distributions) are also available at  [http://repo.data.kit.edu/](http://repo.data.kit.edu/). Please 
refer to your distribution's documentation on how to install a downloaded package.

After installation continue with the [configuration](config.md).