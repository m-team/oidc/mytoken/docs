# Configuration
In most cases no configuration is needed. If you want / need to do configurations create one of the following 
directories:

- `~/.config/mytoken`
- `~/.mytoken`

This directory is your configuration directory.
Copy the example configuration file that was installed with the mytoken package to your configuration directory and name it `config.yaml`.

    cp /usr/share/docs/mytoken/example-config.yaml ~/.config/mytoken # or ~/.mytoken

Now edit the configuration file. You will find comments explaining the different options.
In the following some additional notes on some options:

- `default_token_capabilities`: When requesting a new mytoken you can specify which
  [capabilities](../concepts/capabilities.md) the mytoken should have. If you do 
  not want to provide these everytime you can configure a set of default capabilities.
   
