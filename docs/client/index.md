# The Mytoken Command Line Client

On this page we describe how to use the `mytoken` client to interact with the mytoken server.

We assume that you know what a mytoken is, how it can be used and that they have capabilities and restrictions. If not,
you can read all of this under [Concepts](../concepts/index.md).

The mytoken client is an application that gives you an easy command line interface to interact with a mytoken server.
You can use it to obtain mytoken tokens and OpenID Connect access tokens and more.

The mytoken client does not store or manage mytokens. You have to take care of this yourself, i.e. if you obtain a 
mytoken with the client you are responsable to store it securely. Also, you must pass in a mytoken to use with on 
the possible options.

The mytoken client therefore is especially useful, if the mytoken to be used is provided out-of-band. Users managing 
mytokens are probably better off using [`oidc-agent`](../start/cmd.md#oidc-agent-quickstart).
