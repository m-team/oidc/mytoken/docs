# Usage
## Obtaining a New Mytoken
We will now obtain a new mytoken from an OpenID Connect flow. The mytoken will be printed to stdout (unless `-o` is 
set to something else).

If you have set a default provider in the configuration file you can just run:
```shell
mytoken MT --oidc
```
If you do not pass an already existing mytoken the `--oidc` option can be omitted. But it can be useful to include 
it if WLCG bearer token discovery is used (enabled on default).

If you do not have set a default provider or want to use a non-default one, you have to specify the provider with 
the `-i` option. The option takes a short name of a provider that you have defined in your 
configuration file or the issuer url for that provider.

!!! important
    This token is not restricted. Only create such a powerful if it is needed; if not you should 
    [restrict this token](#restricting-a-mytoken).

!!! info
    This token uses the default capabilities set for this client. If you want to have different capabilities from the 
    default, you can set them with the `--capability` option.

When executing this command, the client will start an OIDC authentication flow. The client prints out an url. You 
have to open it, login, and authorize mytoken. You can do this from any device.

After that, you will be redirected to a success page, which you can close. Your mytoken client should now have obtained
the mytoken and will print it to `stdout`.
This mytoken could now be passed to other applications or stored securely.

## Passing a Mytoken
For most commands a mytoken has to be passed to the client. The only exception is the above described one to obtain 
a new mytoken from OIDC.
Different options to pass a mytoken to the client are available:

- `--MT-file` takes the path to a file that contains the mytoken (in the first line)
- `--MT-env` takes the name of an environment variable populated with the mytoken
- `--MT-prompt` will prompt for the mytoken
- `--MT` takes the mytoken as a command line parameter. However, this is not recommended and one of the other 
   options should be used.
- [WLCG Bearer Token Discovery](https://github.com/WLCG-AuthZ-WG/bearer-token-discovery) is supported. This means 
  that the client will look at the locations defined in this specification if no mytoken is provided.

!!! Important
    If the passed mytoken has token rotation enabled for the request in question, the server will return a new 
    mytoken and the old one will be invalidated.
    
    If the mytoken is passed via a file mytoken client will automatically update it, but with other options this is 
    not possible and the lcient prints a warning including the new mytoken to `stderr`.

In the following we assume that any of the options is used to pass a mytoken and do not display it on the command line.

## Obtaining an Access Token
Obtaining an access token from mytoken is very easy:
```bash
mytoken AT
```

### Requesting `scopes` and `audience`
The above command gives an access token with the scopes and audiences that were configured for the used mytoken.
If no audience restriction was configured, the access token won't have one either. If no 
scopes where configured for the mytoken, all scopes that are supported by the mytoken server for that provider are used.

If we want to have fewer scopes we can request them with the `--scope` (short `-s`) option. We can specify it multiple times to request multiple scopes:
```bash
mytoken AT -s openid -s profile -s email
```

Similar we can request audiences with the `--aud` option.
```bash
mytoken AT --aud="https://storage.example.com/"
```

## Obtaining Mytokens
If a mytoken has the `create_mytoken` [capability](../concepts/capabilities.md) it can be used to
create a (sub-)mytoken. This mytoken could be restricted more than the original, e.g. we can create mytoken that can 
only obtain a limited amount of access token with only specific scopes.
It is common to create sub-mytokens that are passed to other applications, e.g. to start a compute job.

A mytoken can be created from a passed mytoken with the following command.
```shell
mytoken MT
```

If no additional options are passed, the new mytoken will have the same properties as the original mytoken, but we can 
[restrict](#restricting-a-mytoken) the new mytoken more than the original, which can be useful especially 
when it is passed around.

On default the mytoken is returned as a long JWT. When using the mytoken somewhere else it might be suitable to
receive the mytoken as a different [token type](../concepts/mytoken.md).

To request a shorter (no JWT) token add the `--token-type=short` option.

If you want to use the token on another machine it can be easily transferred with a transfer code. Add the
`--token-type=transfer` option to obtain the transfer code. On the other machine you can use
```bash
mytoken MT --TC=<TC>
```

to exchange the transfer code `<TC>` into the mytoken.

### Restricting a Mytoken
So far we only explained the creation of unrestricted mytokens. We mentioned restrictions of mytokens and that it
can be useful, but did not explain how to do it. We will catch up on that now.

!!! danger "Unrestricted Mytokens"
    Unrestricted mytokens are very powerful. Generally you should restrict your mytokens and protect them well.
    (Nearly) unrestricted mytokens for local usage with `oidc-agent` are very handy, and since they 
    never leave your machine and are well encrypted this is fine. If you want to do the same with the mytoken client 
    ensure that they are stored securely.

    **When requesting a mytoken that is not stored encrypted or is passed away you usually want to restrict the 
    mytoken as much as possible!**

As described under [Concepts](../concepts/restrictions.md) we can restrict a 
mytoken with a single restriction clause or multiple. If a mytoken has multiple
restriction clauses it can be used if any one matches.

Generally, there are two ways to request a restricted mytoken:

- The `--restriction` option can be used to pass the restriction(s) as a 
  [template](../concepts/profiles.md).
- A number of command line options can be used to easily pass restrictions.

The first option is a general option, but requires you to write or use existing templates. The second option can be
easier to handle, but only allows a single restrictions clause. But, for many use-cases this is enough.

#### Using the `--restriction` option
Using the `--restriction` option you can restrict a mytoken in any way you want (assuming support by the mytoken instance).
It is not limited in any way.
The option takes a template string, which can refernce an existing template or be a plain json restriction.
For the details on templates and how to write down the restrictions, refer to the
[Concepts](../concepts/profiles.md) and
[Technical Details](../dev/technical/profiles.md#restrictions-templates).

!!! tip
    Please note how [time values](#passing-time-values) are handled!


#### Using command line options
Alternatively, a single restriction clause can be passed by using multiple command line options.
There's one command line option for each claim/entry in the restriction clause. You do not need to provide the
`--restriction` option, instead use the following options:

- `--nbf`: Use this option to restrict the time before the token cannot be used.
- `--exp`: Use this option to restrict the time after which the token can no longer be used.
- `--scope`: Use this option to restrict the scopes for access tokens requested with this mytoken.
  Can be passed multiple times to specify multiple values.
- `--aud`: Use this option to restrict the audiences for access tokens requested with this mytoken.
  Can be passed multiple times to specify multiple values.
- `--host`: Use this option to restrict the ip addresses from which this mytoken can be used. The passed string can
  be a single ip address, a subnet address, or a hostname. The special value 'this' will be replaced by the request ip. 
  Can be passed multiple times to specify multiple values.
- `--geo-ip-allow`: Use this option to restrict the countries from which this mytoken can be used. The passed string
  must be a country code, such as `us`. Can be passed multiple times to specify multiple values.
- `--geo-ip-disallow`: Use this option to restrict the countries from which this mytoken can be used. The passed string
  must be a country code, such as `us`. Can be passed multiple times to specify multiple values.
- `--usages-at`: Use this option to restrict how often this token can be used to obtain an access token.
- `--usages-other`: Use this option to restrict how often this token can be used for actions other than obtaining an access token.

!!! tip
    Please read how [time values](#passing-time-values) can be given!

!!! example
    The restriction clause
    ```json
    {
        "nbf":1598918400,
        "exp":1599004800,
        "scope":"compute storage-read storage-write",
        "audience":[
          "https://hpc.example.com",
          "https://storage.example.com"
        ],
        "hosts":[
          "144.115.171.109",
          "144.115.170.0/24",
          "*.data.kit.edu"
        ],
          "usages_AT": 1
      },
    ```
    can be passed with the following command line options:
    ```bash
    --nbf=1598918400 --exp=1599004800 -s compute -s storage-read -s storage-write --aud="https://hpc.example.com" --aud="https://storage.example.com" --host=144.115.171.109 --host=144.115.170.0/24 --host="*.data.kit.edu" --usages-at=1
    ```

#### Passing Time Values
The mytoken client supports some syntax improvements for passing time based values, i.e. `exp` and `nbf`. These syntax
improvements are supported for all ways of passing restrictions to the client, including 
[profiles and templates](../concepts/profiles.md).

The mytoken client supports the following syntax options for passing time based values:

- Unix time stamp: This is the original way. E.g. `1599004800`
- Absolute time formatted as `2006-01-02 15:04` given in the current local timezone, e.g. `2025-02-09 00:00`
- Relative time, e.g. `+3d`

The option to specify a time base value relative to current time can be very useful. Here are the details on the syntax:

- The passed string must start with a `+`.
- The following time units are supported:
    - `y`: years (365 days)
    - `w`: weeks (7 days)
    - `d`: days (24 hours)
    - `h`: hours
    - `m`: minutes
    - `s`: seconds
- In the special case where only seconds are used the `s` can be omitted.

In the following are some examples for relative time strings:

- `+2w`: 2 weeks from now
- `+3d12h30m`: 3 days, 12 hours and 30 minutes from now
- `+60`, `+60s`, `+1m`: 60 seconds = 1 minute from now


!!! important
    When writing json (in a template) you must pass the time-based values as a string. This means that
    ```json
    {
        "exp": 1599004800
    }
    ```
    becomes
    ```json
    {
        "exp": "1599004800"
    }
    ```
    if you give the value as an absolute unix time stamp, or one of these options:
    ```json
    {
        "exp": "2020-09-02 00:00"
    }
    ``` 
    (The following option assumes it is used on 2020-09-02 00:00:00)
    ```json 
    {
        "exp": "+1d"
    }
    ```

## Revoking a Mytoken
We can revoke a mytoken with the `revoke` command:
```bash
mytoken revoke
```
