# OpenID Connect
The mytoken service is heavily connected to OpenID Connect (OIDC). That is why we want to explain the basics here.
If you are familiar with OIDC skip this page and go directly to [The Mytoken](mytoken.md).

OIDC gives users a way to log in to different sites using a single account at an identity provider.
If users want to access another site, they are redirected to the OpenID provider where they log-in and then are
redirected to the target site. The target site obtains all relevant information from the OpenID provider through
different tokens.

## OpenID Connect Tokens
OIDC knows different tokens that all have different purposes. In the following we will briefly describe these tokens.

### Access Tokens
!!! summary
    Access Tokens are short-lived credentials that are used as authorization for accessing protected resources.
    They can be passed around.

Access tokens are the most important tokens in our context. They are used for authorization at a resource server.
This means that a client that wants access to a resource - e.g. an image on a cloud storage - presents an access token
to the cloud storage server in order to get the image.
The cloud storage server will verify the access token and if everything checks out return the image.

Access tokens can be used for different operations, depending on the resource server and the [scope](#scopes) of the token.

So an access token can be used to actually do something. However, because anyone with the access token can do that something, the lifetime of such an access token is strongly limited, usually in the range of minutes up to an hour.
This means, that after that time the access token can no longer be used, and a new access token is required.

### Refresh Tokens
!!! summary
    Refresh Tokens are long-lived credentials a client can use to obtain additional Access Tokens. Refresh Tokens are
    powerful, must be kept secret, and cannot be passed around.

Sometimes it is necessary for a client to do operations for a longer period than the lifetime of a single access token,
e.g. if a user wants to stay logged-in in a web application.
In such situations the user does not want to re-log-in each time an access token expired, so another menchaism is needed.
(This is even more relevant in use cases where the user is not present anymore, and the application should do things on his behalf.)

Therefore, the client requests a refresh token from the OpenID provider when the user logs-in in the first place. Now
the client does not only receive an access token from the OpenID provider but also a refresh_token.
The refresh token can then be used to obtain a new valid access token when the current one expired. This way the client
can always have a valid access token.

For this to work the refresh token must be long-lived. This is the case, often the refresh token does not expire at all.
Because the refresh token is so powerful, since it allows a client to obtain access tokens for a generally unlimited time,
these refresh tokens are bound to the client that requested them.
This means that - other than access tokens - refresh tokens cannot be passed around.

### ID Tokens
ID Tokens encode user information, that can be used by the client, e.g. the webpage gets the user's email address from the provider.

In the `mytoken` context ID Tokens are not used.

## Scopes
!!! summary
    The `scope` of an Access Tokens defines **what** can be done with that token.

As noted above, access tokens can be used to do different things. **What** can be done with a particular access token is
controlled through a mechanism called `scope`. An access token has one or more scopes attached to it.

There are some well-known, predefined scopes, like `openid`, `email`, `profile`, `address`, `phone`.
The `openid` scope gives access to an identifier string, the `profile` scope access to basic profile information, and
the `email` scope access to the user's email address.
However, clients and resource servers can define and require their own scope values.

!!! example
    An HPC cluster for example could require an access token with the `compute.create` scope in order to create / start
    compute job, while the `compute.cancel` scope is needed to cancel the job.


## Audiences
!!! summary
    The `audience` of an Access Tokens defines **where** it can be used.

While scopes can be used to limit *what* can be done with an access token, `audience` can be used to limit *where* an
access token can be used. Therefore, one or more audiences are encoded in the token.
When a resource server obtains a request with an access token, the server checks if the access token can be used at this
resource, i.e. it checks if it is listed as an audience.

The exact value of an audience claim is not defined. It is recommended to use the url of the resource server, but it
could also be something more general. The important aspect is that the resource server accepts the audience value. 

!!! example
    A cloud storage server could decide to accept the following audiences:
    
    - `https://store.cloud.example.com`
    - `https://store.cloud.example.com/user/dir`
    - `example-cloud`

!!! warning
    Note that currently only very few providers support requesting access tokens with specific audience values.
