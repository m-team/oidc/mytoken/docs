# Mytoken Concepts

In the following we explain the different concepts used by `mytoken`, most importantly the `mytoken` token.
We focus on the aspects relevant for using the mytoken service.
For technical details please additionally refer to [Technical details](../dev/technical/index.md).