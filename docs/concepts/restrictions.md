# Restrictions
!!! summary
  `Restrictions` are a powerful and flexible concept to limit the power of `mytokens` to only the needed privileges.

A mytoken is similar to an OIDC refresh token, since both can be used to obtain access tokens.
However, we designed `mytokens` so that they can be passed around. This makes them extremely useful and powerful,
but also brings risks if a `mytoken` gets stolen.
Therefore, `mytokens` can (and should) be restricted. There are multiple aspects how `mytokens` can be restricted,
and you as the user have full control over all restrictions.

`Mytokens` can be restricted with all of the following properties:

- `nbf`: The mytoken cannot be used before this time. `nbf` and `exp` give a timespan within which the token can be used.
- `exp`: The mytoken cannot be used after this time. `nbf` and `exp` give a timespan within which the token can be used.
- `scope`: When requesting access tokens with this mytoken, only these scopes can be requested.
- `audience`: When requesting access tokens with this mytoken, only access tokens for these audiences can be requested.
- `hosts`: The Mytoken can only be used if the request's ip address matches one the address, subnets, or hostnames 
  specified. The special value `this` will be replaced with the ip from which the request was sent.
- `geoip_allow`: Mytoken will use an ip geo-location database and only allow requests from these countries.
- `geoip_disallow`: Mytoken will use an ip geo-location database and does not allow requests from these countries.
- `usages_AT`: Restricts how often the mytoken can be used to obtain an access token.
- `usages_other`: Restricts how often the mytoken can be used for actions other than obtaining an access token.

!!! example
    All of these can be combined in a restriction *clause*, like the following:
    ```json
    {
        "exp": 1640347200,
        "geoip_allow": ["de"],
        "scope": "openid profile",
        "usages_AT": 1
    }
    ```

    This clause means that the token can only be used before 2021-12-24 12:00pm (UTC). **And** it can only be used from
    Germany. **And** only a single time to obtain an access token with the `openid profile` scope.

As it can be seen from the above example, all the claims in a restriction clause must be fulfilled in order to use the token.
!!! info
    Note that in this example the token can only be used a single time to request an access token, but the usage for other
    actions (if they are allowed by the token's capabilities) are not restricted in their number.

A mytoken can have no, one, or multiple restriction clauses:

- If it has none, the token is unrestricted and can be used from anywhere, at any time, for any action allowed by the token's capabilities.
- If it has only one restriction clause, the token can only be used if this restriction clause is fulfilled.
- If it has multiple clauses, the token can be used if any of the restriction clauses matches.

!!! warning
    Unrestricted mytokens are powerful. There are use cases where unrestricted mytokens are useful and accepted
    (however, they must be stored securely); but generally mytokens should always be restricted in some way.

!!! example
    The following is an example of a `restrictions` with two clauses:
    ```json
    [
         {
            "nbf":1598918400,
            "exp":1599004800,
            "scope":"compute storage.read storage.write",
            "audience":[
              "https://hpc.example.com",
              "https://storage.example.com"
            ],
            "hosts":[
              "144.115.171.109",
              "144.115.170.0/24",
              "*.data.kit.edu"
            ],
            "usages_AT": 1,
            "usages_other": 0
          },
          {
            "nbf":1598918400,
            "exp":1599523200,
            "scope":"storage.write",
            "audience":[
              "https://storage.example.com"
            ],
            "hosts":[
              "144.115.171.109",
              "144.115.170.0/24",
              "*.data.kit.edu"
            ],
            "usages_other": 0
          }
    ]
    ```

    This allows the token to be used:

    - From 2020-09-01 00:00:00 until 2020-09-02 00:00:00 from the ip 144.114.171.109 and the subnet 144.115.170.0/24 and ips under the `*.data.kit.edu` domain
      to obtain a single access token with the `compute`, `storage.read`, and `storage.write` scopes and the `https://hpc.example.com`
      and `https://storage.example.com` audiences.
    - From 2020-09-01 00:00:00 until 2020-09-08 00:00:00 from the ip 144.114.171.109 and the subnet 144.115.170.0/24 and ips under the `*.data.kit.edu` domain
      to obtain (multiple) access tokens with the `storage.write` scope and the `https://storage.example.com` audience.

The above example could be useful when submitting a compute job. First an access token with the `compute` scope is required
to start the job at `https://hpc.example.com`. The same access token that also has the `storage.read` scope can be used
at `https://storage.example.com` to load some data. After one day (or after the access token was already obtain) this is
no longer possible.
The second clause allows to obtain a new access token at the end of the job to write back the results.
The access token can have the `storage.write` scope to write the results back to `https://storage.example.com` within 7 days.
