# The Mytoken
The mytoken service introduces a new powerful token type which we also called `mytoken`.
In the following we will describe in detail everything you need to know about the mytoken token and its different representations.

!!! summary
    Mytokens can be used at the mytoken server to obtain OIDC access tokens, but also for other operations such as creating sub-mytokens.

Mytokens are the tokens you use with the mytoken service, and the primary use of a mytoken is to obtain OIDC access
tokens from the mytoken server.
In that aspect mytokens are similar to [OIDC refresh tokens](oidc.md#refresh-tokens): You need to authenticate once with
an OIDC flow and then can use the mytoken to obtain fresh access tokens whenever you need one.
However, unlike OIDC refresh tokens, mytokens can be passed around.
This is one of the basic design principles behind mytokens: We designed them, so that obtaining access tokens from any
machine is as easy as possible.
Also, mytokens are more secure than refresh tokens because you can enforce 
additional [restrictions](restrictions.md).

## The JWT Mytoken
!!! summary
    Usually mytokens are represented as a JWT that encodes all important information about the token.

Mytokens can be represented in different ways. Usually they are represented as a JSON Web Token (JWT).
This means that all relevant data about the token is encoded in the token (as the *payload*), and the token also
contains a cryptographic signature of that payload.
This helps the server to verify that a mytoken is valid, but also clients can benefit from the JWT format, because they
can look into the token and use the information to improve user experience. For example, they easily can say when a
mytoken is expired, or check what actions can be done with a mytoken.


## The Short Mytoken
!!! summary
    A `short mytoken` is a much shorter but opaque token.

If a mytoken is represented as a JWT there is a lot of information encoded in the token. Together with the cryptographic
signature this is a substantial amount of data which means that a JWT mytoken can become quite long.
Of course, you don't have to remember the token (you should manage your token in another way, e.g. with the
[mytoken client](../client/index.md)). But there are still cases where the length can 
be an issue and a shorter token is needed.

If a JWT mytoken is too long for a certain use case, a `short mytoken` can be used instead. As the name suggests the
short token is much shorter, e.g. only 64 characters instead of over 1000.
With this reduced length it is usually possible to use it wherever needed. 
A short mytoken can be used just the same way as the JWT mytoken, the mytoken server will take care of it.

Since the short mytoken is not a JWT, but an opaque string, clients can no longer decode it to obtain information about
the mytoken. However, they can still obtain information about the mytoken 
from the mytoken server's [tokeninfo endpoint](../dev/api/latest/endpoints/tokeninfo.md).

## The Transfer Code
!!! summary
    A transfer code is a very short one-time code that can be used to transfer a mytoken.

`Transfer Codes` are a bit different from JWT or short mytokens, because they cannot be used just the same way.
Instead, a transfer code can be used to transfer a mytoken to another machine, e.g. from your desktop to your notebook,
or from your desktop to a server.

!!! example
    Assuming you have a mytoken on your desktop and want to copy it to your notebook. This can be done with three easy steps:

    - Create a transfer code for the existing mytoken on your desktop. You will obtain a very short code.
    - Copy this code to your notebook (it is so short that you can just type it).
    - Exchange the transfer code into the mytoken on the notebook.

Transfer codes are very short, have a limited lifetime and can only be used once. It is possible to create transfer
codes for already existing mytokens (as in the example above), but it is also possible to obtain a transfer code
directly when creating a new mytoken.

## Technical Details
See [Technical details](../dev/technical/mytoken.md) for technical information 
about how mytokens work.