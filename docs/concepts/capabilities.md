## Capabilities
!!! summary
    A mytoken can only be used for the actions allowed by its *capabilities*.

While the primary usage of a mytoken is to obtain access tokens, it can also be used for other actions. Similar to
[OIDC scopes](oidc.md#scopes) the capabilities of a mytoken define the allowed 
actions for that token.

These are the defined capabilities and the actions they allow:

### `AT`
This can be considered the 'standard' capability. A mytoken with the `AT` capability can be used to obtain access tokens.

### `create_mytoken`
A mytoken with the `create_mytoken` capability can be used to create a new (sub-)mytoken. The new mytoken cannot be more
powerful than the original token.
  
!!! example
    - If the original token expires in 7 days, the new sub-token cannot be valid for a longer period.
    - A created sub-mytoken can only have capabilities that the original mytoken already has.

### `tokeninfo`
The `tokeninfo` capability is a "parent"-capability for all capabilities that start with `tokeninfo:`, i.e. the 
following three ones. A mytoken with the `tokeninfo` capability can be used for all actions that are allowed by any 
of these capabilities.

#### `tokeninfo:introspect`:
A mytoken with the `tokeninfo:history` capability can be used to obtain basic information about itself. This includes
the token's content (useful for [short tokens](mytoken.md#the-short-mytoken))
and if [restrictions](restrictions.md) are
defined that limit the number of usages this also allows to query how often the mytoken was already used.
Introspection calls are not counted as usages with regard to the restriction.

#### `tokeninfo:history`
A mytoken with the `tokeninfo:history` capability can be used to obtain the event history for that token, i.e. this
allows you to see how / when this token was used.

#### `tokeninfo:subtokens`
A mytoken with the `tokeninfo:subtokens` capability can be used to get information about all subtokens (and their 
subtokens and so on).

### `list_mytokens`
A mytoken with the `list_mytokens` capability can be used to list the revocation id and some basic information about 
all your existing mytokens.

### `revoke_any_token`
A mytoken with the `revoke_any_token` capability can revoke any mytoken by its revocation id.
Normally, a mytoken can only revoke itself or a subtoken. This capability allows to also revoke other tokens.

### `settings`
A mytoken with the `settings` capability can be used to change user settings, in particular it allows control over your 
so called `grant types` - these are the methods how you authorize.
    
!!! hint
    At the moment the `settings` capability primarily allows control over the
    [*ssh grant type*](../dev/technical/ssh.md). In future versions the power of 
    this capability might be extended.

!!! important
    The `settings` capability is - like the `tokeninfo` capability - a capability that has sub-capabilities. A 
    mytoken with the `settings` capability will also have all its sub-capabilities (and theirs). Therefore, and 
    because these capabilities are sensitive the `settings` capability is a really powerful one.

#### `read@settings`
Allows read access to all user settings, i.e. it includes all capabilities starting with `read@settings:`.

#### `settings:grants`
A Mytoken with the `settings:grants` capability can be used to make changes to the user grants. This means that 
additional grant types (ways to obtain mytokens) can be enabled, disabled, and configured.

Currently, only the `ssh` user grant is available.

##### `read@settings:grants`
Allows read access to the user grants.

##### `settings:grants:ssh`
A mytoken with the `settings:grants:ssh` capability can be used to enable, disable, and configure the `ssh` user grant.

###### `read@settings:grants:ssh`
Allows read access to the `ssh` user grant, i.e. it allows to query the list of enabled ssh keys and if the grant 
type itself is enabled or not.