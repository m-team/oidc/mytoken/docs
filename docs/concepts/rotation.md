# Token Rotation
!!! summary
    Token Rotation allows single-use mytokens and automatic abuse detection.

Token rotation means that the token is rotated on each usage, i.e. if token rotation is enabled for a mytoken and the
token is used this mytoken will not be valid anymore, instead a new mytoken is returned. This means that each mytoken
of this "chain" can only be used once.
Therefore, token rotation greatly reduces the risk that a stolen mytoken can be abused. Furthermore, the mytoken server
can detect that a token is used by multiple persons and automatically revoke the mytoken.

Token Rotation can be enabled / disabled on a per token basis, this way it can be enabled for clients that support it,
but clients that cannot handle rotating tokens can use non-rotating mytokens.

!!! info
    A client must be able to store back the mytoken it uses (i.e. to update the stored mytoken), in order to support
    token rotation.

!!! danger "Parallel Access"
    Token rotation cannot be used if a single mytoken is used in parallel without synchronization. E.g. from multiple
    devices where each device keeps it own copy of the used mytoken.

A user can decide if a mytoken should be rotated and on what type of requests. Also users can define whether the server
should automatically revoke a mytoken chain if it detects an abuse.

Additionally, a `lifetime` can be set that limits how long each individual mytoken of the chain can live.
By combining the rotation `lifetime` and the [restriction's](restrictions.md)
`exp` claim an (in)definitely renewable mytoken can be created.
This means it is possible to create a mytoken chain where each individual mytoken is valid
for a limited amount of time, e.g. 7days (this is the rotation lifetime), but if the token is used within that time, a
new mytoken is returned that again can be used for 7d.
However, if a mytoken in this chain is not used within that 7 days, it will be
expired and there is no way how it can be used anymore and also no new mytoken can be obtained from it.

While the rotation lifetime allows renewable, short-lived individual mytokens, the restriction's `exp` claim can be used
to set a definite expiration time for the whole mytoken chain.

!!! example 
    This way it is possible to have a mytoken chain, that can be used for one year with rotating mytokens where each one
    can only be used for a single week.

For technical details about how token rotation is implemented please refer to our page about
[technical details](../dev/technical/rotation.md).
