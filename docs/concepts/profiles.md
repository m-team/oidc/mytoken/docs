# Profiles And Templates
!!! summary
    Mytoken profiles and templates allow to pass properties of a mytoken in a shorter an easier way by referecing 
    pre-defined values.

!!! attention
    With mytoken server `0.7.0` there was a significant change in the profile concept. Previously, profile and 
    templates were a client-side concept and only relevant for clients (mytoken client, oidc-agent). With server 
    version `0.7.0` this changed: Profiles (and templates) moved to the server. Client side profiles are no 
    longer supported since mytoken client `0.6.0` and oidc-agent `4.5.0`.

Mytoken profiles and templates are a way to pass the properties of a mytoken in an easier and more compact way.
To achieve this, parts of the request are pre-written and stored as a template / profile on the server. These 
profiles can be re-used and referenced in requests.

Templates exist for `capabilities`, `restrictions`, and `rotation`. The profile is on a higher level and can 
contain all properties of a mytoken request and also reference templates.

Users cannot create own templates, but use all existing ones. There are predefined templates and profiles on a 
mytoken server and communities can be allowed to create their own. To do so contact the administrators of the 
mytoken server and see [the technical information](../dev/technical/profiles.md) for details.

Profiles and templates can be used in all API requests where this makes sense. Using a profile in a request has to 
comply to the [same rules as writing a profile](../dev/technical/profiles.md).