# Mytokens
Mytokens are the key element of the mytoken service. In the following we describe the technical and implementation details.

## The Mytoken in General
Before explaining the details of the different representations some general notes:

- Additionally to the data encoded in the mytoken the following data about a mytoken is stored in the database:
    - Parent and root relationship information (for sub-tokens)
    - Metadata (creation time, creation IP, ...)
    - Current Sequence number (for [token rotation](rotation.md))
    - Event History
- All mytokens are linked to an OIDC refresh token
    - Multiple mytokens can be linked to the same RT (this is the case for sub-tokens)
    - Mytokens created from an OIDC flow are linked to the obtained RT
    - Subtokens inherit the RT of their parent
    - Refresh tokens are stored encrypted with the mytoken's JWT representation used (indirectly) as the encryption
      password (see [Refresh Token Encryption](encryption.md) for details)
- The data encoded in the mytoken is listed in the [section about the JWT representation](#the-jwt-mytoken) together
  with details how we create the values for each entry

## Representations

There are different representations for a mytoken, but all mytokens are created as a JWT and other representations are a
proxy for the JWT. 

### The JWT Mytoken

The following is an example mytoken (line wraps for display purposes only):
!!! example
    ```
    eyJhbGciOiJFUzUxMiIsInR5cCI6IkpXVCJ9.eyJ2ZXIiOiIwLjMiLCJ0b2tlbl90eXBlIjoibXl0b2tlbiIsImlzcyI6Imh0d
    HBzOi8vbXl0b2tlbi5leGFtcGxlLmNvbSIsInN1YiI6IkUwQ3IwTlRWdUJWR0pvNXd2cnk0YTYzcU0zQWVkbGdrK2lzZVhUVUV
    PUHc9IiwiZXhwIjoxNjcyNDgxNDcxLCJuYmYiOjE2NDM3NjM3MjIsImlhdCI6MTYzNDExNjU4OSwianRpIjoiNTBiYjNhOTctN
    TQ4MC00ZmY1LTkzNzMtMjU1ODliYjhkYWIyIiwic2VxX25vIjoxLCJuYW1lIjoiRXhhbXBsZSIsImF1ZCI6Imh0dHBzOi8vbXl
    0b2tlbi5leGFtcGxlLmNvbSIsIm9pZGNfc3ViIjoiZXhhbXBsZSIsIm9pZGNfaXNzIjoiaHR0cHM6Ly9vcC5leGFtcGxlLmNvb
    SIsInJlc3RyaWN0aW9ucyI6W3sibmJmIjoxNjQzNzYzNzIyLCJleHAiOjE2NzI0ODE0NzEsInNjb3BlIjoib3BlbmlkIHByb2Z
    pbGUgZW1haWwgZWR1cGVyc29uX2VudGl0bGVtZW50IiwidXNhZ2VzX0FUIjoxfV0sImNhcGFiaWxpdGllcyI6WyJjcmVhdGVfb
    Xl0b2tlbiIsInRva2VuaW5mb19pbnRyb3NwZWN0IiwidG9rZW5pbmZvX3RyZWUiLCJ0b2tlbmluZm9faGlzdG9yeSJdLCJzdWJ
    0b2tlbl9jYXBhYmlsaXRpZXMiOlsiQVQiXSwicm90YXRpb24iOnsib25fQVQiOnRydWUsImxpZmV0aW1lIjoxMDAwMDAwLCJhd
    XRvX3Jldm9rZSI6dHJ1ZX19.AZ1MWI1LfIs1Z8Kzn0b7x230RLmm1SfAfPfmOLvydjrqncg6Ow3KVgJx12kwlUk43_g11reB7U
    3E_LnuMJpBjcnJAA_BcO1IgtHRGp0hlxL7SK7KtD6EERvSu67UQyFbVWkwEsdA195vlVKK7K9FovQbUNA6Io3KGHzQe5nIJiwJ
    Taxa
    ```

#### Payload
A mytoken encodes all important information about a mytoken.
The mytoken from the example above encodes the following information:
!!! example
    ```json
    {
        "ver": "0.3",
        "token_type": "mytoken",
        "iss": "https://mytoken.example.com",
        "sub": "E0Cr0NTVuBVGJo5wvry4a63qM3Aedlgk+iseXTUEOPw=",
        "exp": 1672481471,
        "nbf": 1643763722,
        "iat": 1634116589,
        "jti": "50bb3a97-5480-4ff5-9373-25589bb8dab2",
        "seq_no": 1,
        "name": "Example",
        "aud": "https://mytoken.example.com",
        "oidc_sub": "example",
        "oidc_iss": "https://op.example.com",
        "restrictions": [
            {
                "nbf": 1643763722,
                "exp": 1672481471,
                "scope": "openid profile email eduperson_entitlement",
                "usages_AT": 1
            }
        ],
        "capabilities": [
            "create_mytoken",
            "tokeninfo_introspect",
            "tokeninfo_tree",
            "tokeninfo_history"
        ],
        "subtoken_capabilities": [
            "AT"
        ],
        "rotation": {
            "on_AT": true,
            "lifetime": 1000000,
            "auto_revoke": true
        }
    }
    ```

Generally a mytoken can have the following fields:

- `ver`: The version of the mytoken scheme used.
- `token_type`: Indicates that this is a mytoken; used to differentiate mytokens from other tokens such as access tokens. Is always set to `mytoken`.
- `iss`: The URL of the instance that issued this mytoken.
- `sub`: A subject identifier uniquely identifying the user. We construct it by hashing a combination of the `oidc_sub` and `oidc_iss`.
- `exp`: The time when the mytoken expires. We automatically populate this field from the `restrictions`.
- `nbf`: The time when the mytoken can be used for the first time. Unless the `restrictions` define that the mytoken can only be used later, this is equal to `iat`.
- `iat`: The time when the mytoken was issued.
- `auth_time`: The time when the user performed the OIDC flow to obtain the linked OIDC token.
- `jti`: A random and uniquely identifier for this mytoken. We use v4 UUIDs.
- `seq_no`: The tokens sequence number. Always starts at `1` and increments on [token rotation](rotation.md).
- `name`: The name of this mytoken.
- `aud`: The audience of the token; this is equal to `iss`.
- `oidc_sub`: The user's `sub` at the `oidc_iss`.
- `oidc_iss`: The OIDC issuer url.
- `restrictions`: A JSON array of [restrictions objects](../../concepts/restrictions.md) defining the mytoken's restrictions (see [restrictions](restrictions.md) for the technical details). 
- `capabilities`: A JSON array of the [capabilities](../../concepts/capabilities.md) 
  allowed for this token (see [capabilities](capabilities.md) for the technical details).
- `subtoken_capabilities`: A JSON array of the [capabilities](../../concepts/capabilities.md) allowed for subtokens of this token (see 
  [capabilities](capabilities.md) for the technical details).
- `rotation`: A rotation object defining the [rotation policy](../../concepts/rotation.md) for this mytoken (see [rotation](rotation.md) 
  for the technical details).

#### Signature
Our implementation of the mytoken server supports different signing algorithms:

- `RS256`, `RS384`, `RS512`
- `PS256`, `PS384`, `PS512`
- `ES256`, `ES384`, `ES512`

At [https://mytoken.data.kit.edu](https://mytoken.data.kit.edu) we use `ES512`.

### The Short Mytoken

Short Tokens are created as a random string. It serves as what we call a `Proxy Token` as it proxies the JWT mytoken.
Internally, the mytoken servers always creates the JWT mytoken first. When a short mytoken is created the JWT mytoken is
encrypted with the short token, and the encrypted JWT mytoken is stored in the database.
When a request includes a short mytoken, the server obtains the corresponding encrypted JWT from the database and uses
the short token to decrypt it. Then the server proceeds as if the decrypted JWT had been sent directly.

All `Proxy Tokens` follow the principle that the proxy token is used for encrypting / decrypting the token that is
proxied while the hash value is used as an identifier for matching entries in the database.

### The Transfer Code
Transfer codes are also `Proxy Tokens`, i.e. the mytoken that they proxy (i.e. transfer) is stored encrypted with the
transfer code. The proxied mytoken can be a JWT mytoken or a short mytoken the `Proxy Token` concept handles the proxied
tokens as opaque objects.

Compared to a short mytoken a transfer code enforces the limited lifetime 
and one-time usage.
Implementation wise transfer codes are very close to `polling codes`.