# Profile and Templates

Profiles and templates are written as `json` and stored on the server.

They can be used in all API requests where this makes sense. Using a profile in a request has to
comply to the [same rules as writing a profile](#general-rules).

## Creating Profiles
In the following we describe how to create own profiles and templates. The next section then describes the rules for 
writing profiles and templates.

### Requirements
It is not intended that individuals define their own profiles. Usually, there is a number of predefined profiles and 
templates available on a mytoken server.
Some organizations or communities might want to define their own profiles for mytoken specifications that are often 
used within their community.

To do so they must contact the administrators of a mytoken server. The administrators can create profile group 
credentials. These credentials are needed to create, update, or delete profiles within that group.
All profiles and templates can always be read and used by everyone with access to a mytoken server.

### Tool Support
In the [mytoken server GitHub repo](https://github.com/oidc-mytoken/server/tree/prerel/config/server-profiles) there 
is a directory `config/server-profiles`.
This directory contains a `update-profiles.sh` script as well as a directory structure with predefined profiles and 
templates.

To use this tool checkout the git repository and navigate to that directory or download it in another way.
!!! example
    ```shell-session
    git clone https://github.com/oidc-mytoken/server mytoken-server
    cd mytoken-server/config/server-profiles
    ```

Use the existing directory structure (or recreate it somewhere else) and define the profiles and templates you wish 
to add. The tool can also be used to update already existing entries.

To create / update the server data from the local entries run the script and pass the url of the mytoken server and 
your group credentials:
```shell-session
./update-profiles.sh -s <mytoken_server_url> -u <group_username> -p <group_password>
```


### API
Instead or in addition to the above described tool, the API can also be used directly. The details of the profile 
endpoint are described under [API](../api/latest/endpoints/profiles.md).

## General Rules
There are some general rules applicable to all template types and profiles, but also some special rules for certain
types.
In the following we write "profiles" but this is also applicable for all templates.

- Profiles are written as json, and have a name.
- Profiles can include other profiles through the `include` claim.
    - The `include` claim can be a JSON Array or space delimited list (also a single value).
    - When including profiles the name CAN be prefixed with the `@` character.
    - When including profiles from the same or another group the full name has to be used, i.e. 
      `<group_name>/<profile_name>`
    - Only when including profiles from the `_` default group the group part in the name can be omitted.
- When including profiles, attributes not yet specified are included - attributes already specified are not
  overwritten.

## Capability Templates
The capability template is the only template type that is not a JSON **object** - it is a JSON array. Therefore, it has 
a few special rules, because it cannot use the `include` claim.

In a capability template other templates are included by using the template name with a `@` prefix. In capability
templates the prefix is NOT OPTIONAL and no `include` claim is used (there is no json object).

The following are examples for valid templates:

- `#!json ["AT", "create_MT", "tokeninfo"]`
- `#!json ["AT create_MT", "tokeninfo"]`
- `#!json ["@basic", "create_MT"]`

## Rotation Templates
The rotation template content is a JSON object as it is used in the [API](../api/latest/types/rotation.md).
Other templates can be included with the `include` claim.

Assuming the following rotation templates exists:

- `kit/AT`: `#!json {"on_AT": true}`
- `kit/daily`: `#!json {"lifetime": 86400}`
- `kit/revoke`: `#!json {"auto_revoke": true}`
- `kit/revoke-daily`: `#!json {"include": "kit/daily kit/revoke"}`

The following template
```json
{
  "on_other": true,
  "include": ["kit/AT", "kit/revoke-daily"]
}
```
will produce the final rotation policy:
```json
{
  "on_other": true,
  "on_AT": true,
  "lifetime": 86400,
  "auto_revoke": true
}
```

## Restrictions Templates
A restrictions template can contain a single restriction clause as a JSON object or multiple in a JSON Array.

The following is an example of a template and the resulting restrictions:

Assuming the following restrictions exist:

- `kit/1d`: `#!json {"exp":"+1d"}`
- `kit/1w`: `#!json {"exp":"+1w"}`
- `kit/ip-cluster`: `#!json {"hosts":["111.222.111.0/24"]}`
- `kit/get-data`:
    ```json
    {
      "scope": "openid profile storage.read",
      "aud": ["https://storage.example.com"],
      "include": "kit/ip-cluster kit/1d"
    }
    ```
- `kit/store-data`:
    ```json
    {
      "scope": "openid profile storage.write",
      "aud": ["https://storage.example.com"],
      "include": "kit/ip-cluster kit/1w"
    }
    ```

The template content
```json
[{
  "scope": "openid profile compute.submit",
  "aud": ["https://hpc.example.com"],
  "usages_AT": 1
}, {
  "usages_AT": 1,
  "include": "kit/get-data"
},
  "@kit/store-data"
]
```

will result in the following restrictions:
```json
[{
  "scope": "openid profile compute.submit",
  "aud": ["https://hpc.example.com"],
  "usages_AT": 1
}, {
  "usages_AT": 1,
  "scope": "openid profile storage.read",
  "aud": ["https://storage.example.com"],
  "hosts":["111.222.111.0/24"],
  "exp": "+1d"
}, {
  "scope": "openid profile storage.write",
  "aud": ["https://storage.example.com"],
  "hosts":["111.222.111.0/24"],
  "exp": "+1w"
}]
```

## Profiles
As already noted profiles are a higher level concept than templates, but can use them.

The following is a simple example how a profile could look like:
```json
{
  "name": "example mytoken",
  "max_token_len": 1000,
  "rotation": {
    "on_AT": true,
    "include": "rotate-daily"
  },
  "capabilities": "@default create_MT",
  "restrictions": [
    {
     "scope": "openid profile",
      "exp": "+2d",
      "include": "ip-this"
    },
    "kit/storage-get"
  ]
}
```
