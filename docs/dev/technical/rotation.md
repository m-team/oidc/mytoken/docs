# Token Rotation
On this page we want to explain some technical details about how token rotation works in the mytoken server.

## Rotating the Mytokens
Each mytoken (chain) has an ID (the `jti` in the JWT). This id is the same for all mytokens in the same chain and will
not change.
Each (individual) mytoken also has a sequence number (`seq_no` in the JWT). The `seq_no` always starts at `1` what also
means that all mytokens that do not use token rotation always have a `seq_no` of `1`.

If rotation is enabled, all mytokens in a chain have the same ID, but the `seq_no` increases with each rotation. Since
mytokens are JWTs, the `seq_no` is encoded in the mytoken and can be seen if the token is decoded.
This way the server can easily create new, non-predictable, but still linked mytoken strings on each rotation.
This implementation also allows the server to verify very easily that a mytoken is the newest in a chain by keeping
track of the current `seq_no` for each mytoken.

## Automatic Revocation
This `seq_no` is also used to detect token abuse:
If the `seq_no` of a mytoken is smaller than the one stored in the database (but the signature still checks out), the
presented mytoken is an old one, that is no longer valid, and the server can revoke the whole chain.
