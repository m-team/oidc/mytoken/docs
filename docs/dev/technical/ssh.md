---
title: SSH
---

# The SSH Grant Type

Mytoken server `0.4.0` adds support for the `ssh` grant type, i.e. it is possible to obtain mytokens and
access tokens through `ssh`.

## Adding a New SSH Key
When users add a new ssh key they must upload the public key as well as perform the OIDC authorization code flow. The
OIDC flow gives the mytoken server a refresh token, this refresh token is linked to a mytoken as usual. To be more
precise each ssh key has an underlying mytoken. When the ssh key is added the user can set the capabilities and
restrictions of this mytoken; this way defining how the ssh key can be used.

The server also creates a new random ssh username which is linked to the ssh public key in the database. We do not
store the whole ssh public key, but only the SHA256 fingerprint. The server also does not store the username, but only a 
hash value, because the plain username is used for encrypting the linked mytoken.
The username is returned to the user and they must store it securely, since it is needed for sshing and we cannot
restore it.

## Using SSH
When the ssh grant type is used the server performs various checks:

- Assert that the used public key is known and linked to the ssh username
- Assert that the (mytoken) user linked to the key has enabled the ssh grant type

The server also obtains the linked mytoken and decrypts it with the ssh username. It then fills the mytoken into the
requests and then handles this request.


