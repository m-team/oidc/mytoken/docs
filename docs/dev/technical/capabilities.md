# Capabilities

From the technical side `capabilities` are quite simple:

- The `capabilities` which are allowed for a mytoken are encoded inside the JWT.
- When the server receives a request it checks that the needed capability is present in the mytoken.