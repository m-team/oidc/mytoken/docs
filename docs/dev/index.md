# Resources for Developers
Here you can find resources for developers and reviewers.
In the following we describe [technical details](technical/index.md) of the mytoken 
server and also detail the [API](api/index.md). 