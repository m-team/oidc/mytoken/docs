# Endpoints
In the following we describe the different endpoints and available operations.

!!! important
    If a mytoken is used that has token rotation enabled, the response to any request can contain an updated Mytoken.
    To be more precise: If the mytoken was rotated (depending on the policy), the response MUST contain the updated mytoken.
    The updated mytoken is returned in the `updated_token` claim. This claim contains a whole mytoken response object as
    described in [the mytoken endpoint](mytoken.md#mytoken-response).

!!! important
    For all requests a mytoken can also be passed in the Authorization header or `mytoken` cookie, instead of the
    `mytoken` request body parameter. However, it cannot be passed as a query parameter.