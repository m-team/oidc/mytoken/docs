# Restrictions

`Mytokens` can become very powerful if not restricted. The `restrictions` type can be used to restrict a `mytoken` to
make it more secure. Restrictions allow users to narrow down a mytoken to just the actions that are needed for the particular use-case.

If restricted, a `Mytoken` has a JSON Array of `restriction clauses` defined. The token can be used when any of the
`restriction clauses` in the array matches. A `restriction clause` is a JSON Object that combines different types of
restrictions (e.g. temporal and location-based). A `restriction clause` only matches if all contained `restriction claims` are fulfilled.

A `restriction clause` might contain the following `restriction claims`:

| Claim | Type | Description |
| ----- | ---- | ----------- |
| `nbf` | A numeric date value (UNIX timestamp) | Mytoken cannot be used before this time. `nbf` and `exp` give a timespan within which the Mytoken can be used. |
| `exp` | A numeric date value (UNIX timestamp) | Mytoken cannot be used after this time. `nbf` and `exp` give a timespan within which the Mytoken can be used. |
| `scope` | A space separated list of OIDC scopes. | This restricts the scopes OIDC access tokens obtain with this mytoken can have. |
| `audience` | A JSON Array of audience values. | Only Access Tokens with the specified audiences (or a subset) can be requested. |
| `ip` | A JSON Array of ip addresses or subnets. | The Mytoken can only be used from the provided ip addresses or subnets. Each entry in the array can be a single ip or subnet. |
| `geoip_allow` | A JSON Array of short country codes (`de`, `us`, etc.) | The Mytoken can only be used if the source ip geo-locates to one of the countries in the array. |
| `geoip_disallow` | A JSON Array of short country codes (`de`, `us`, etc.) | The Mytoken cannot be used if the source ip geo-locates to one of the countries in the array. |
| `usages_AT` | A numerical value indicating how often the Mytoken can be used to obtain ATs | The Mytoken can only be used to obtain ATs if it was used less often in the past as allowed. Only usages at the Access Token Endpoint are counted. |
| `usages_other` | A numerical value indicating how often the Mytoken can be used for other purposes than obtaining ATs | The Mytoken can only be used if it was used less often in the past as allowed. Only usages other than at the Access-Token-Endpoint are counted. |

!!! example
    Assuming a Mytoken with the following restrictions:
    ```json
    [
        {
            "nbf":1598918400,
            "exp":1599004800,
            "scope":"compute storage.read storage.write",
            "audience":[
                "https://hpc.example.com",
                "https://storage.example.com"
            ],
            "ip":[
                "144.115.171.109",
                "144.115.170.0/24"
            ],
            "usages_AT": 1
        },
        {
            "nbf":1598918400,
            "exp":1599523200,
            "scope":"storage.write",
            "audience":[
                "https://storage.example.com"
            ],
            "ip":[
                "144.115.171.109",
                "144.115.170.0/24"
            ]
        }
    ]
    ```

    This Mytoken can be used:

    * From `2020-09-01 00:00:00` until `2020-09-02 00:00:00`
    from the ip `144.114.171.109` and the subnet `144.115.170.0/24`
    to obtain one access token with the `compute`, `storage.read` and `storage.write` scopes
    and the `https://hpc.example.com` and `https://storage.example.com` audience.
    * From `2020-09-01 00:00:00` until `2020-09-08 00:00:00`
    from the ip `144.114.171.109` and the subnet `144.115.170.0/24`
    to obtain access tokens with the `storage-write` scope
    and the `https://storage.example.com` audience.

