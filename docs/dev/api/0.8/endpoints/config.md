# Configuration Endpoint

The URL of the configuration endpoint is constructed by appending `/.well-known/mytoken-configuration` to the server's
issuer url. This is analogous to how it is done with the OIDC configuration endpoint.
A server SHOULD also provide the same metadata or a reduced and adapted subset at the url of the OIDC configuration
endpoint, for compatibility with OIDC.
A mytoken server MUST provide a configuration endpoint. 

## Configuration Metadata Request
The client sends a GET request to the server's configuration endpoint.

!!! example
    ```http
    GET /.well-known/mytoken-configuration HTTP/1.1
    Host: mytoken.example.com
    ```

## Configuration Metadata Response
The response uses the `application/json` media type.
The following metadata values are defined:

| Parameter                                     | Necessity                              | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
|-----------------------------------------------|----------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `issuer`                                      | REQUIRED                               | URL using the https scheme with no query or fragment component that the mytoken instance asserts as its Issuer Identifier. This MUST be identical to the iss Claim value in Tokens issued from this mytoken instance.                                                                                                                                                                                                                                                                                                                                                                                    |
| `access_token_endpoint`                       | REQUIRED                               | URL of the mytoken’s Access Token Endpoint.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `mytoken_endpoint`                            | REQUIRED                               | URL of the mytoken’s Mytoken Endpoint.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| `tokeninfo_endpoint`                          | RECOMMENDED                            | URL of the mytoken’s Token Info Endpoint.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| `revocation_endpoint`                         | RECOMMENDED                            | URL of the mytoken’s Token Revocation Endpoint.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| `usersettings_endpoint`                       | REQUIRED                               | URL of the mytoken’s User Settings Endpoint.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| `token_transfer_endpoint`                     | RECOMMENDED                            | URL of the mytoken’s Token Transfer Endpoint.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| `jwks_uri`                                    | REQUIRED                               | URL of the mytoken’s [JSON Web Key Set](https://openid.net/specs/openid-connect-discovery-1_0.html#JWK)document. This contains the signing key(s) a client MIGHT use to validate signatures from mytoken.                                                                                                                                                                                                                                                                                                                                                                                                |
| `ssh_keys`                                    | REQUIRED if `ssh` grant type supported | A JSON array containing JSON Objects describing the server's ssh public keys used for the `ssh` grant type.  Each entry MUST have the following metadata values: <table><thead><tr><th>Claim</th><th>Description</th></tr></thead><tbody><tr><td>`type`</td><td>The type of this ssh key, e.g. `ssh-rsa`</td></tr><tr><td>`fingerprint`</td><td>The `SHA256` fingerprint of this public key.</td></tr></tbody></table>                                                                                                                                                                                   |
| `providers_supported`                         | REQUIRED                               | A JSON array containing JSON Objects describing the supported OpenID Connect providers. Each entry MUST have the following metadata values: <table><thead><tr><th>Claim</th><th>Description</th></tr></thead><tbody><tr><td>`issuer`</td><td>URL using the https scheme with no query or fragment component that the OpenID Provider asserts as its Issuer Identifier. This MUST be identical to the `iss` Claim value in ID Tokens issued from this provider.</td></tr><tr><td>`scopes_supported`</td><td>JSON array containing a list of scopes supported for this provider.</td></tr></tbody></table> |
| `token_signing_alg_value`                     | REQUIRED                               | The JWS signing algorithm (alg values) used by mytoken to encode the Claims in a JWT.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| `tokeninfo_endpoint_actions_supported`        | REQUIRED if any supported              | JSON array containing a list of actions supported at the tokeninfo endpoint.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| `access_token_endpoint_grant_types_supported` | REQUIRED                               | JSON array containing a list of mytoken Grant Types supported by this Access Token Endpoint. The only option is `mytoken`. Other authentication methods MAY be defined in the future.                                                                                                                                                                                                                                                                                                                                                                                                                    |
| `mytoken_endpoint_grant_types_supported`      | REQUIRED                               | JSON array containing a list of mytoken Grant Types supported by the Mytoken Endpoint. The options are `mytoken`, `oidc_flow`, `polling_code`, `ssh`. `mytoken` and `oidc_flow` MUST be supported. Other authentication methods MAY be defined in the future.                                                                                                                                                                                                                                                                                                                                            |
| `mytoken_endpoint_oidc_flows_supported`       | REQUIRED                               | JSON array containing a list of OpenID Connect flows that are supported as authentication mechanisms for Mytoken requests. The only option is `authorization_code` and MUST be supported. Other OpenID Connect flow values MAY be defined in the future.                                                                                                                                                                                                                                                                                                                                                 |
| `response_types_supported`                    | REQUIRED                               | JSON array containing a list of the supported response types on the Mytoken endpoint. Possible values are `token`, `short_token`, `transfer_code`. ‘token’ MUST be supported.                                                                                                                                                                                                                                                                                                                                                                                                                            |
| `service_documentation`                       | OPTIONAL                               | URL of a page containing human-readable information that developers might want or need to know when using the mytoken service.                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| `version`                                     | RECOMMEND                              | The mytoken server version, this instance runs.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| `restriction_claims_supported`                | REQUIRED                               | A JSON array of the keys that can be used within a restriction clause.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |

!!! example
    ```http
    HTTP/1.1 200 OK
    Content-Type: application/json

    {
      "issuer": "https://mytoken.example.com",
      "access_token_endpoint": "https://mytoken.example.com/api/v0/token/access",
      "mytoken_endpoint": "https://mytoken.example.com/api/v0/token/my",
      "tokeninfo_endpoint": "https://mytoken.example.com/api/v0/tokeninfo",
      "revocation_endpoint": "https://mytoken.example.com/api/v0/token/revoke",
      "usersettings_endpoint": "https://mytoken.example.com/api/v0/settings",
      "token_transfer_endpoint": "https://mytoken.example.com/api/v0/token/transfer",
      "jwks_uri": "https://mytoken.example.com/jwks",
      "providers_supported": [
        {
          "issuer": "https://wlcg.cloud.cnaf.infn.it/",
          "scopes_supported": [
            "openid",
            "profile",
            "email",
            "storage.create:/",
            "storage.read:/",
            "storage.modify:/",
            "compute.read",
            "compute.modify",
            "compute.create",
            "compute.cancel",
            "wlcg",
            "wlcg.groups",
            "eduperson_scoped_affiliation",
            "eduperson_entitlement"
          ]
        },
        {
          "issuer": "https://aai.egi.eu/auth/realms/egi",
          "scopes_supported": [
            "openid",
            "profile",
            "email",
            "eduperson_entitlement",
            "eduperson_scoped_affiliation",
            "eduperson_unique_id",
            "cert_entitlement",
            "orcid",
            "ssh_public_key"
          ]
        },
        {
          "issuer": "https://login.helmholtz.de/oauth2",
          "scopes_supported": [
            "openid",
            "profile",
            "email",
            "display_name",
            "credentials",
            "eduperson_entitlement",
            "eduperson_scoped_affiliation",
            "eduperson_assurance",
            "eduperson_unique_id",
            "eduperson_principal_name",
            "sn",
            "single-logout"
          ]
        }
      ],
      "token_signing_alg_value": "ES512",
      "service_documentation": "https://mytoken-docs.data.kit.edu",
      "version": "0.6.0",
      "tokeninfo_endpoint_actions_supported": [
        "introspect",
        "event_history",
        "subtokens",
        "list_mytokens"
      ],
      "access_token_endpoint_grant_types_supported": [
        "mytoken"
      ],
      "mytoken_endpoint_grant_types_supported": [
        "oidc_flow",
        "mytoken",
        "transfer_code",
        "polling_code",
        "ssh"
      ],
      "mytoken_endpoint_oidc_flows_supported": [
        "authorization_code"
      ],
      "response_types_supported": [
        "token",
        "short_token",
        "transfer_code"
      ],
      "supported_restriction_keys": [
        "nbf",
        "exp",
        "scope",
        "audience",
        "ip",
        "geoip_allow",
        "geoip_disallow",
        "usages_AT",
        "usages_other"
      ],
      "token_endpoint": "https://mytoken.example.com/api/v0/token/access"
    }
    ```