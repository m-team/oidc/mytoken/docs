# Notifications
The notifications endpoint is used to manage a user's notifications.
The URI of the notification endpoint can be obtained from the [configuration endpoint](../config.md).

## Get Notification List
### Notifications List Request
To get a list of the user's notifications, the client sends a GET request to the notification endpoint.
The request MUST include a mytoken with the `read@manage_mytokens:notify` capability as authorization.

!!! example
    ```http
    GET /api/v0/notifications HTTP/1.1
    Host: mytoken.example.com
    Authorization: Bearer eyjsaiend...
    ```

### Notifications List Response
A successful response returns the following parameters using the `application/json` media type:

| Parameter       | Necessity | Description                                                                      |
|-----------------|-----------|----------------------------------------------------------------------------------|
| `notifications` | REQUIRED  | A JSON array of notification objects holding information about each notification |

The notification object has the following attributes:

| Parameter              | Necessity                                           | Description                                                                 |
|------------------------|-----------------------------------------------------|-----------------------------------------------------------------------------|
| `notification_id`      | REQUIRED                                            | A numeric unique id                                                         |
| `notification_type`    | REQUIRED                                            | The type of the notification; one of `mail`, `ws`                           |
| `management_code`      | REQUIRED                                            | A management code that can be used to manage this notification              |
| `user_wide`            | REQUIRED                                            | A bool indicating if this is a user wide notification                       |
| `ws`                   | REQUIRED if `notification_type=ws`                  | The web socket path linked to this notification                             |
| `notification_classes` | REQUIRED                                            | A JSON Array of the subscribed notification classes                         |
| `subscribed_tokens`    | REQUIRED if tokens are subscribed and not user wide | A list of the mom ids of the tokens <br/>that are included in this calendar |


!!! example
    ```http
    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "notifications": [
            {
                "notification_id": 32,
                "notification_type": "mail",
                "management_code": "xqbhff8TJoPtEzpYNwJbeplXbaJdYkjd8ASdk3jAJDke3c8WoBfXF70o3BlueEva",
                "user_wide": true,
                "notification_classes": [
                    "expiration",
                    "security"
                ]
            },
            {
                "notification_id": 31,
                "notification_type": "mail",
                "management_code": "1pT4qnsqULbtXFUsi03IpV881jdsksdlksjdajienvjnsdh348AJS3zQMFF5BeSC",
                "user_wide": false,
                "notification_classes": [
                    "setting_changes",
                    "AT_creations"
                ],
                "subscribed_tokens": [
                    "2Ia3ttmk3DBUZ/YihLIimRld44eG6GSEa2xS1bGerrO7ExFhF+Ul3XGzP4tY+wnSsuODSIgz03QBqhBGyEFDfw==",
                    "4Xh80antRyB1m9e5jB5B71ePYnXa3WxzprQuhiHVoKvQ8Ch/+IMQbYfO6bxxspspvIJ6fPOwqL7neMM/XuMfaQ==",
                    "ZRlNhr38hm1GInyDDI5xk7aurn1hIGeqKB9MMbxHOtxSTzKyjDOo13yvNTAdF6KfLWnJv5SCCufIiuRQurBCfQ=="
                ]
            }
        ]
    }
    ```

## Create New Notification
To create a new notification the client sends a POST request to the notification endpoint.
The request parameters depend on the notification type.
The following notifications can be created this way:

- Email Notifications
- Calendar invite via email for Expiration of a mytoken
- Notifications through a web socket (NYI)

### Email Notifications
#### New Email Notification Request
To create a new email notification the client sends a POST request to the notification endpoint and adds the
following parameters using the `application/json` or `application/x-www-form-urlencoded` format
in the HTTP request entity-body:

| Parameter              | Necessity | Description                                                                                                                                                                          |
|------------------------|-----------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `mytoken`              | REQUIRED  | The mytoken in subject of the notification or an authorization mytoken                                                                                                               |
| `notification_type`    | REQUIRED  | MUST be `mail`                                                                                                                                                                       |
| `mom_id`               | OPTIONAL  | If given the mytoken associated with this mom id is the subject of the notification, not the token from `mytoken`                                                                    |
| `notification_classes` | REQUIRED  | A JSON Array specifying which notification classes should be subscribed. For <br/>details about the different notification classes see [Notification Classes](#notification-classes) |
| `user_wide`            | OPTIONAL  | If `true` this notification does not apply to only specific mytokens, but instead to all mytoken's of the user                                                                       |
| `include_children`     | OPTIONAL  | If `true` also current and future children (recursively) of the subject token are added to the notification                                                                          |

If a `mom_id` is given the token in `mytoken` is used as authorization and MUST have the `manage_mytokens:notify` capability.
If `mom_id` is not given, the notification will be created for the token in `mytoken`, which MUST have the 
`tokeninfo:notify` capability.


!!! example
    ```http
    POST /api/v0/notifications/ HTTP/1.1
    Host: mytoken.example.com
    Content-Type: application/json

    {
        "mytoken": "eyJhbGcio...",
        "mom_id": "Kqi9Jvwz5v4QCpNGrx8oon5ZJmtQEZ+zK/s52ynWpUMkl4kjEMskeLL3RqTkhvC6FOk7mTs1tJPdLXPx8OZ+uQ==",
        "notification_type": "mail",
        "notification_classes": [
            "expiration",
            "security"
        ],
        "include_children": true
    }
    ```

#### Notification Classes
The following notification classes are defined:

| Notification Class                     | Description                                                                                | Comment                                                                                                                                  |
|----------------------------------------|--------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------|
| `AT_creations`                         | Notifications for the creation of access tokens                                            |                                                                                                                                          |
| `subtoken_creations`                   | Notifications for the creation of mytoken subtokens                                        | If used in user-wide notification this includes creation of all mytokens                                                                 |
| `setting_changes`                      | Notifications for changes in the settings                                                  |                                                                                                                                          |
| `security`                             | Notifications for all security related events                                              | Includes all `security:*` notification classes                                                                                           |
| `security:blocked_usages`              | Notifications for all blocked usages                                                       | Includes all `security:blocked_usages:*` notification classes                                                                            |
| `security:blocked_usages:capabilities` | Notifications for blocked usages because of insufficient capabilities                      |                                                                                                                                          |
| `security:blocked_usages:restrictions` | Notifications for blocked usages because of restrictions                                   |                                                                                                                                          |
| `security:revoked`                     | Notifications for tried usage of a revoked tokens (only works as a user-wide notification) |                                                                                                                                          |
| `security:ips`                         | Notifications for usages from previously unknown ip addresses                              | Notifies if a mytoken is used from an ip address where it was not yet used from                                                          |
| `expiration`                           | Notifications before a mytoken expires                                                     | Notifies in advance, depending on the lifetime of the token; notification also contains a link to re-create a similar configured mytoken |

#### New Email Notification Response

A successful response returns the following parameters using the `application/json` media type:

| Parameter         | Necessity | Description                                                                     |
|-------------------|-----------|---------------------------------------------------------------------------------|
| `management_code` | REQUIRED  | A management code that can be used to view, alter, and delete the notification. |


!!! example
    ```http
    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "management_code": "BwZsbqS6JwKbrOj9XobDYbE9c1HmSDylEcasdf4kDooxQ8OFXSb7gPZ4YDnof7Xp"
    }
    ```


### Calendar Invitation
#### Calendar Invite Request
To create a calendar event invitation the client sends a POST request to the notification endpoint and adds the 
following parameters using the `application/json` or `application/x-www-form-urlencoded` format
in the HTTP request entity-body:

| Parameter           | Necessity | Description                                                                                                       |
|---------------------|-----------|-------------------------------------------------------------------------------------------------------------------|
| `mytoken`           | REQUIRED  | The mytoken in subject of the notification or an authorization mytoken                                            |
| `notification_type` | REQUIRED  | MUST be `ics_invite`                                                                                              |
| `mom_id`            | OPTIONAL  | If given the mytoken associated with this mom id is the subject of the notification, not the token from `mytoken` |
| `comment`           | OPTIONAL  | A comment that, if present, will be included in the expiration event's description                                |

If a `mom_id` is given the token in `mytoken` is used as authorization and either MUST be a parent of the 
`mom_id`-token and having the `tokeninfo:notify` capability or have the `manage_mytokens:notify` capability.

!!! example
    ```http
    POST /api/v0/notifications/ HTTP/1.1
    Host: mytoken.example.com
    Content-Type: application/json

    {
        "mytoken": "eyJhbGcio...",
        "mom_id": "Kqi9Jvwz5v4QCpNGrx8oon5ZJmtQEZ+zK/s52ynWpUMkl4kjEMskeLL3RqTkhvC6FOk7mTs1tJPdLXPx8OZ+uQ==",
        "notification_type": "ics_invite"
    }
    ```
    
#### Calendar Invite Response
A successful response has the HTTP status code `204` and no content, unless the used mytoken has been rotated, in this
case the updated mytoken is returned with a status code of `200`.


## Managing a Notification
The base url endpoint for managing notifications is constructed by appending a notification's management code to the 
notification endpoint. We refer to this url as notification management endpoint.

### Get Notification Information
#### Notification Information Request
To obtain information abut a notification by its management url the client sends a GET request to the notification 
management endpoint.

!!! example
    ```http
    GET /api/v0/notifications/BwZsbqS6JwKbrOj9XobDYbE9c1HmSDylEcasdf4kDooxQ8OFXSb7gPZ4YDnof7Xp HTTP/1.1
    Host: mytoken.example.com
    ```

#### Notification Information Response
A successful response returns the following parameters using the `application/json` media type:

| Parameter              | Necessity                                           | Description                                                                             |
|------------------------|-----------------------------------------------------|-----------------------------------------------------------------------------------------|
| `notification_id`      | REQUIRED                                            | A numeric unique id                                                                     |
| `notification_type`    | REQUIRED                                            | The type of the notification; one of `mail`, `ws`                                       |
| `management_code`      | REQUIRED                                            | A management code that can be used to manage this notification                          |
| `user_wide`            | REQUIRED                                            | A bool indicating if this is a user wide notification                                   |
| `ws`                   | REQUIRED if `notification_type=ws`                  | The web socket path linked to this notification                                         |
| `notification_classes` | REQUIRED                                            | A JSON Array of the subscribed notification classes                                     |
| `subscribed_tokens`    | REQUIRED if tokens are subscribed and not user wide | A list of the mom ids of the tokens <br/>that are included in this calendar             |
| `oidc_iss`             | REQUIRED                                            | The issuer url of the OpenID Provider that is linked to the tokens of this notification | 


!!! example
    ```http
    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "notification_id": 32,
        "notification_type": "mail",
        "management_code": "BwZsbqS6JwKbrOj9XobDYbE9c1HmSDylEcasdf4kDooxQ8OFXSb7gPZ4YDnof7Xp",
        "subscribed_tokens": [
            "Kqi9Jvwz5v4QCpNGrx8oon5ZJmtQEZ+zK/s52ynWpUMkl4kjEMskeLL3RqTkhvC6FOk7mTs1tJPdLXPx8OZ+uQ==",
            "4Xh80antRyB1m9e5jB5B71ePYnXa3WxzprQuhiHVoKvQ8Ch/+IMQbYfO6bxxspspvIJ6fPOwqL7neMM/XuMfaQ=="
        ],
        "notification_classes": [
            "expiration",
            "security"
        ],
        "oidc_iss": "https://op.example.com"
    }
    ```

### Delete Notification
#### Notification Deletion Request
To delete a notification the client sends a GET request to the notification management endpoint.

!!! example
    ```http
    DELETE /api/v0/notifications/BwZsbqS6JwKbrOj9XobDYbE9c1HmSDylEcasdf4kDooxQ8OFXSb7gPZ4YDnof7Xp HTTP/1.1
    Host: mytoken.example.com
    ```

#### Notification Deletion Response
A successful response has the HTTP status code `204` and no content.

### Update Notification
#### Update Notification Classes
##### Update Notification Classes Request
To update (add or remove) the notification classes for a notification the client appends `/nc` to the notification 
management 
endpoint and sends a POST or PUT request to this endpoint, adding the following parameters using the `application/json` or 
`application/x-www-form-urlencoded` format in the HTTP request entity-body:

| Parameter              | Necessity | Description                                                                                |
|------------------------|-----------|--------------------------------------------------------------------------------------------|
| `notification_classes` | REQUIRED  | A JSON Array of the notification classes. The current classes will be replaced with these. |

The server will replace the currently subscribed notification classes with the one from the request, i.e. all 
classes that should apply to the notification must be included in the request.

!!! example
    ```http
    PUT /api/v0/notifications/BwZsbqS6JwKbrOj9XobDYbE9c1HmSDylEcasdf4kDooxQ8OFXSb7gPZ4YDnof7Xp/nc HTTP/1.1
    Host: mytoken.example.com

    {
        "notification_classes": [
            "security"
        ]
    }
    ```

##### Update Notification Classes Response
A successful response has the HTTP status code `204` and no content.

#### Add Token
##### Add Token Request
To add a token to a notification the client appends `/token` to the notification management
endpoint and sends a POST request to this endpoint, adding the following parameters using the `application/json` or
`application/x-www-form-urlencoded` format in the HTTP request entity-body:

| Parameter          | Necessity                        | Description                                                                                         |
|--------------------|----------------------------------|-----------------------------------------------------------------------------------------------------|
| `mytoken`          | REQUIRED, if `mom_id` not given  | The mytoken that should be added to the notification                                                |
| `mom_id`           | REQUIRED, if `mytoken` not given | The mom id of the mytoken that should be added to the notification                                  |
| `include_children` | OPTIONAL                         | If `true` also current and future children (recursively) of the token are added to the notification |

!!! example
    ```http
    POST /api/v0/notifications/BwZsbqS6JwKbrOj9XobDYbE9c1HmSDylEcasdf4kDooxQ8OFXSb7gPZ4YDnof7Xp/token HTTP/1.1
    Host: mytoken.example.com

    {
        "mom_id": "ZRlNhr38hm1GInyDDI5xk7aurn1hIGeqKB9MMbxHOtxSTzKyjDOo13yvNTAdF6KfLWnJv5SCCufIiuRQurBCfQ==",
        "include_children": true
    }
    ```

##### Add Token Response
A successful response has the HTTP status code `204` and no content.

#### Remove Token
##### Remove Token Request
To remove a token from a notification the client appends `/token` to the notification management
endpoint and sends a DELETE request to this endpoint, adding the following parameters using the `application/json` or
`application/x-www-form-urlencoded` format in the HTTP request entity-body:

| Parameter          | Necessity                        | Description                                                          |
|--------------------|----------------------------------|----------------------------------------------------------------------|
| `mytoken`          | REQUIRED, if `mom_id` not given  | The mytoken that should be removed to the notification               |
| `mom_id`           | REQUIRED, if `mytoken` not given | The mom id of the mytoken that should be removed to the notification |

!!! example
    ```http
    DELETE /api/v0/notifications/BwZsbqS6JwKbrOj9XobDYbE9c1HmSDylEcasdf4kDooxQ8OFXSb7gPZ4YDnof7Xp/token HTTP/1.1
    Host: mytoken.example.com

    {
        "mom_id": "ZRlNhr38hm1GInyDDI5xk7aurn1hIGeqKB9MMbxHOtxSTzKyjDOo13yvNTAdF6KfLWnJv5SCCufIiuRQurBCfQ=="
    }
    ```

##### Remove Token Response
A successful response has the HTTP status code `204` and no content.
