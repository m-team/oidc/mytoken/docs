# Calendars

The calendar endpoint is used to manage a user's calendars.
The URI of the calendar endpoint is constructed by appending `/calendars` to the [notification endpoint](index.md) which 
can be obtained from the [configuration endpoint](../config.md).

## Get Calendar List
### Calendar List Request
To get a list of the user's calendars, the client sends a GET request to the calendar endpoint.
The request MUST include a mytoken with the `read@manage_mytokens:notify` capability as authorization.

!!! example
    ```http
    GET /api/v0/notifications/calendars HTTP/1.1
    Host: mytoken.example.com
    Authorization: Bearer eyjsaiend...
    ```

### Calendar List Response
A successful response returns the following parameters using the `application/json` media type:

| Parameter   | Necessity | Description                                                              |
|-------------|-----------|--------------------------------------------------------------------------|
| `calendars` | REQUIRED  | A JSON array of calendar objects holding information about each calendar |

The calendar object has the following attributes:

| Parameter           | Necessity                         | Description                                                            |
|---------------------|-----------------------------------|------------------------------------------------------------------------|
| `name`              | REQUIRED                          | The name of the calendar                                               |
| `ics_paths`         | REQUIRED                          | An URI where the calendar's ics can be downloaded                      |
| `subscribed_tokens` | REQUIRED if tokens are subscribed | A list of the mom ids of the tokens that are included in this calendar |

!!! example
    ```http
    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "calendars": [
            {
                "name": "foobar",
                "ics_path": "https://mytoken.example.com/calendars/59ckwFkBdQoqo6t5jAkdnek24kd9HvUB",
                "subscribed_tokens": [
                    "rB8g1NhHqRNQL8S38DkzQgMJ9Df7wVmnuX1dkSk3kAlsUQM6igudDWL3ZMYf4xm1AL8/pQdGAetBa/y5ktydrg==",
                    "Kqi9Jvwz5v4QCpNGrx8oon5ZJmtQEZ+zK/s52ynWpUMkl4kjEMskeLL3RqTkhvC6FOk7mTs1tJPdLXPx8OZ+uQ==",
                    "1/nvsonJaTN8krm3CfiIpX0AbvG3+XVo5a//YO3Eiu2yfUkEcbZKNjaDsQpvYjd39r6SiiAg62pdOdqwTZo58w==",
                    "nhuqkrFWpstfwoxI5DczUiIG52Bg20WEXAuHzm3W2Csyp7dhhpLcmwRb9yPQ3hwxla44aTyncpqDkej6PNy7oQ=="
                ]
            },
            {
                "name": "test",
                "ics_path": "https://mytoken.example.com/calendars/wKDX3j1jal5kDkD2k4Jkl4tfunVJHlxw"
            },
        ]
    }
    ```


## Create Calendar
### Create Calendar Request
To create a new calendar, the client sends a POST request to the calendar endpoint and adds the
following parameters using the `application/json` or `application/x-www-form-urlencoded` format in the HTTP request entity-body:

| Parameter | Necessity | Description                                                                        |
|-----------|-----------|------------------------------------------------------------------------------------|
| `mytoken` | REQUIRED  | A mytoken used as authorization; MUST have the `manage_mytokens:notify` capability |
| `name`    | REQUIRED  | The name of the calendar                                                           |

!!! example
    ```http
    POST /api/v0/notifications/calendars HTTP/1.1
    Host: mytoken.example.com
    Content-Type: application/json

    {
        "name": "CI Mytokens",
        "mytoken": "eyJhbGcio..."
    }
    ```

### Create Calendar Response
A successful response has the HTTP status code `201` and returns a single calendar object as described under 
[Calendar List Response](#calendar-list-response).

!!! example
    ```http
    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "name": "CI Mytokens",
        "ics_path": "https://mytoken.example.com/calendars/59ckwFkBdQojad93Daks3jk24kd9HvUB"
    }
    ```

## Get Calendars ICS
### Calendar Info Request
To get a calendars ics data a client sends a GET request to its `ics_path`.
If the client did not already obtain the `ics_path` it can send a GET request to the calendar endpoint suffixed by the 
calendar name.
This request MUST include a mytoken with the `read@manage_mytokens:notify` capability as authorization.

!!! example
    ```http
    GET /api/v0/notifications/calendars/foobar HTTP/1.1
    Host: mytoken.example.com
    Authorization: Bearer eyjsaiend...
    ```

### Calendar Info Response
A successful response will redirect to the `ics_path` which will return the ics data.


## Add Token to Calendar
### Add Token Request
To add a mytoken to a calendar the client sends a POST request to the calendar endpoint suffixed by the calendar 
name and adds the following parameters using the `application/json` or `application/x-www-form-urlencoded` format
in the HTTP request entity-body:

| Parameter | Necessity | Description                                                                        |
|-----------|-----------|------------------------------------------------------------------------------------|
| `mytoken` | REQUIRED  | A mytoken used as authorization; MUST have the `manage_mytokens:notify` capability |
| `mom_id`  | REQUIRED  | The mom id of the mytoken that should be added                                     |
| `comment` | OPTIONAL  | A comment that, if present, will be included in the expiration event's description |

!!! example
    ```http
    POST /api/v0/notifications/calendars/foobar HTTP/1.1
    Host: mytoken.example.com
    Content-Type: application/json

    {
        "mytoken": "eyJhbGcio...",
        "mom_id": "Kqi9Jvwz5v4QCpNGrx8oon5ZJmtQEZ+zK/s52ynWpUMkl4kjEMskeLL3RqTkhvC6FOk7mTs1tJPdLXPx8OZ+uQ=="
    }
    ```

## Add Token Response
A successful response has the HTTP status code `200` and returns a single calendar object as described under
[Calendar List Response](#calendar-list-response).

!!! example
    ```http
    HTTP/1.1 200 OK
    Content-Type: application/json

     {
        "name": "foobar",
        "ics_path": "https://mytoken.example.com/calendars/59ckwFkBdQoqo6t5jAkdnek24kd9HvUB",
        "subscribed_tokens": [
            "rB8g1NhHqRNQL8S38DkzQgMJ9Df7wVmnuX1dkSk3kAlsUQM6igudDWL3ZMYf4xm1AL8/pQdGAetBa/y5ktydrg==",
            "Kqi9Jvwz5v4QCpNGrx8oon5ZJmtQEZ+zK/s52ynWpUMkl4kjEMskeLL3RqTkhvC6FOk7mTs1tJPdLXPx8OZ+uQ==",
            "1/nvsonJaTN8krm3CfiIpX0AbvG3+XVo5a//YO3Eiu2yfUkEcbZKNjaDsQpvYjd39r6SiiAg62pdOdqwTZo58w==",
            "nhuqkrFWpstfwoxI5DczUiIG52Bg20WEXAuHzm3W2Csyp7dhhpLcmwRb9yPQ3hwxla44aTyncpqDkej6PNy7oQ=="
        ]
    }
    ```


## Delete Calendar
### Delete Calendar Request
To delete a calendar the client sends a DELETE request to the calendar endpoint suffixed by the calendar name.
The request MUST include a mytoken with the `manage_mytokens:notify` capability as authorization.

!!! example
    ```http
    DELETE /api/v0/notifications/calendars/foobar HTTP/1.1
    Host: mytoken.example.com
    Authorization: Bearer eyjsaiend...
    ```

### Delete Calendar Response
A successful response has the HTTP status code `204` and no content, unless the used mytoken has been rotated, in this
case the updated mytoken is returned with a status code of `200`.

