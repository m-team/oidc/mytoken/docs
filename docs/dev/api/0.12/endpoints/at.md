# Access Token Endpoint

The access token endpoint is an endpoint to obtain OpenID Connect Access Tokens and similar to the OpenID Provider's
token endpoint.
The access token endpoint can be used with a mytoken as the OIDC token endpoint can be sued with a refresh token.

## Access Token Request
To obtain an OIDC access token the client makes an access token request to the access token endpoint by adding the
following parameters using the `application/json` or `application/x-www-form-urlencoded` format in the HTTP request entity-body:

| Parameter       | Necessity                                 | Description                                                                                                                                                                                                  |
|-----------------|-------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `oidc_issuer`   | OPTIONAL                                  | The issuer url of the OpenID Provider                                                                                                                                                                        |
| `grant_type`    | REQUIRED.                                 | MUST be `mytoken` or `refresh_token`                                                                                                                                                                         |
| `mytoken`       | REQUIRED, unless `refresh_token` is given | The mytoken that should be transferred                                                                                                                                                                       |
| `refresh_token` | REQUIRED if `mytoken` not given           | This is an alias for `mytoken` to be compatible with OIDC.                                                                                                                                                   |
| `scope`         | RECOMMENDED                               | The scopes for the requested access token as a space separated list; if omitted all scopes allowed by the used mytoken are used (if multiple restriction clauses can be used, the first match is used)       |
| `audience`      | OPTIONAL                                  | The audiences for the requested access token as a space separated list; if omitted all audiences allowed by the used mytoken are used (if multiple restriction clauses can be used, the first match is used) |
| `comment`       | OPTIONAL                                  | A comment string indicating the intended usage of the access token                                                                                                                                           |


!!! example
    ```http
    POST /api/v0/token/access HTTP/1.1
    Host: mytoken.example.com
    Content-Type: application/json

    {
        "oidc_issuer": "https://op.example.org",
        "grant_type": "mytoken",
        "mytoken": "eyJhbGcio...",
        "scope": "openid profile eduperson_entitlement"
    }
    ```

## Access Token Response
A successful response is analog to a successful access token response from the authorization server as described in
[OAuth 2 Section 5.1](https://tools.ietf.org/html/rfc6749#section-5.1).
In particular the response returns the following parameters (if possible) using the `application/json` media type:

| Parameter | Necessity | Description |
| --------- | --------- | ----------- |
| `access_token` | REQUIRED | The access token issued by the authorization server |
| `token_type` | REQUIRED | The type of the token issued as described in [OAuth 2 Section 7.1](https://tools.ietf.org/html/rfc6749#section-7.1) |
| `expires_in` | RECOMMENDED | The lifetime in seconds of the access token |
| `scope` | OPTIONAL | The scope of the access token |
| `audience` | OPTIONAL | JSON array indicating the audiences of the access token |

!!! example
    ```http
    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "access_token":"2YotnFZFEjr1zCsicMWpAA",
        "token_type":"Bearer",
        "expires_in":3600,
        "scope": "openid profile"
    }
    ```

