# Token Info Endpoint

The tokeninfo endpoint is an endpoint to obtain various information about a specific Mytoken or all Mytokens.

The endpoint supports the following operations:

- [Introspect](#introspect): Obtain information about this MT: validity, content, usages
- [History](#history): Obtain the event history for this MT
- [Subtokens](#subtokens): Get the subtoken tree for this MT
- [List](#list-mytokens): Lists all Mytokens for this user.

All of these actions require the appropriate capability and can be enabled/disabled by the server administrator for a
mytoken deployment.

## Introspect

This operation requires the `tokeninfo:introspect` capability. It is especially useful for checking if a token is valid
and to "decode" a short mytoken.

### Introspection Request
To introspect a mytoken the client makes an introspection request to the tokeninfo endpoint by adding the
following parameters using the `application/json` or `application/x-www-form-urlencoded` format in the HTTP request entity-body:

| Parameter | Necessity | Description               |
|-----------|-----------|---------------------------|
| `action`  | REQUIRED  | MUST be `introspect`      |
| `mytoken` | REQUIRED  | The mytoken to introspect |

!!! example
    ```http
    POST /api/v0/token/introspect HTTP/1.1
    Host: mytoken.example.com
    Content-Type: application/json

    {
        "action": "introspect",
        "mytoken": "eyJhbGcio..."
    }
    ```

### Introspection Response
A successful response returns the following parameters using the `application/json` media type:

| Parameter    | Necessity   | Description                                        |
|--------------|-------------|----------------------------------------------------|
| `valid`      | REQUIRED    | MUST be `true` iff the token is valid              |
| `token_type` | REQUIRED    | The type of the mytoken (`token` or `short_token`) |
| `token`      | REQUIRED    | The decoded token payload                          |
| `mom_id`     | RECOMMENDED | The mom id of the token                            |

!!! info
    If the token has restrictions that restrict the number of usages and the token was already used, the response MUST
    contain how often the token was already used.
    In this case the restrictions object in the decoded payload will be extended by fields indicating how often the token
    was already used.
    These fields are named: `usages_AT_done` and `usages_other_done`.

!!! example
    ```http
    HTTP/1.1 200 OK
    Content-Type: application/json

    {
      "valid": true,
      "token_type": "short_token",
      "token": {
        "iss": "https://mytoken.example.com",
        "sub": "...",
        "nbf": 1612367394,
        "iat": 1612367394,
        "jti": "d2a4...",
        "aud": "https://mytoken.example.com",
        "oidc_sub": "...",
        "oidc_iss": "https://op.example.org",
        "restrictions": [
          {
            "scope": "openid profile storage.write",
            "usages_AT": 10,
            "usages_AT_done": 2
          },
          {
            "scope": "openid profile storage.read",
            "usages_AT": 10,
            "usages_AT_done": 4
          }
        ],
        "capabilities": [
          "AT",
          "create_mytoken",
          "tokeninfo:introspect",
          "tokeninfo:history",
          "tokeninfo:subtokens"
        ]
      }
    }
    ```


## History

This operation requires the `tokeninfo:history` capability or the `manage_mytokens:history` capability when using a 
mom id.


### History Request
To query the history for a mytoken the client makes a history request to the tokeninfo endpoint by adding the
following parameters using the `application/json` or `application/x-www-form-urlencoded` format in the HTTP request entity-body:

| Parameter | Necessity | Description                                                                                                                                                                                                |
|-----------|-----------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `action`  | REQUIRED  | MUST be `history`                                                                                                                                                                                          |
| `mytoken` | REQUIRED  | The mytoken                                                                                                                                                                                                |
| `mom_ids` | OPTIONAL  | If given, the event history for the mytokens associated with these mom ids is returned, not for the token in the `mytoken` parameter, the `mytoken` parameter is still required and used as authorization. |

!!! important
    In `mom_ids` the special id `this` can be used to request the event history for the mytoken in the `mytoken` 
    parameter. This is usually not needed, but it can be useful if additionally, event histories for other mytokens are 
    requested.

    The special value `children` can be used to request the event history for all the children (recursively) of the 
    mytoken in the `mytoken` parameter. It will not include the event history for the subject token itself.

    The special value `children@<mom_id>` can be used to request the event history for all the children (recursibely)
    of the mytoken with the `<mom_id>`.

!!! attention
    Requesting event history for any mytoken other than itself, requires the authorization mytoken to either be a 
    parent or having the `manage_mytokens:history` capability.

!!! example
    ```http
    POST /api/v0/token/introspect HTTP/1.1
    Host: mytoken.example.com
    Content-Type: application/json

    {
        "action": "history",
        "mytoken": "eyJhbGcio..."
    }
    ```

### History Response
A successful response returns the following parameters using the `application/json` media type:

| Parameter | Necessity                                          | Description                                                 |
|-----------|----------------------------------------------------|-------------------------------------------------------------|
| `events`  | REQUIRED                                           | A JSON Array of event entries, ordered ascending by time    |

Each event entry can have the following parameters:

| Parameter    | Necessity | Description                                                                          |
|--------------|-----------|--------------------------------------------------------------------------------------|
| `event`      | REQUIRED  | A string indicating the event type                                                   |
| `time`       | REQUIRED  | A numerical time value (UNIX timestamp) indicating the time when this event occurred |
| `comment`    | OPTIONAL  | A comment string that gives additional information                                   |
| `ip`         | OPTIONAL  | The IP that was used to trigger the event                                            |
| `user_agent` | OPTIONAL  | The user agent that was used to trigger the event                                    |
| `mom_id`     | REQUIRED  | The mom id of the mytoken that is linked to this event                               |

!!! example
    ```http
    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "events": [
            {
                "event": "created",
                "comment": "Used grant_type oidc_flow authorization_code",
                "ip": "142.42.42.42",
                "user_agent": "Mozilla/5.0(X11;Linuxx86_64;rv:78.0)Gecko/20100101Firefox/78.0",
                "time": 1636373529,
                "mom_id": "ihmvVP/DGX7xyqKPr9wXRP582wyaqgQG8o7adFNQlqk59s6TGmhl/M9v2KY3OrzpuWA/z+LS5gS08gWHres09w=="
            },
            {
                "event": "tokeninfo_introspect",
                "ip": "142.42.42.42",
                "user_agent": "Mozilla/5.0(X11;Linuxx86_64;rv:78.0)Gecko/20100101Firefox/78.0",
                "time": 1636373530,
                "mom_id": "ihmvVP/DGX7xyqKPr9wXRP582wyaqgQG8o7adFNQlqk59s6TGmhl/M9v2KY3OrzpuWA/z+LS5gS08gWHres09w=="
            },
            {
                "event": "token_rotated",
                "ip": "142.42.42.42",
                "user_agent": "Mozilla/5.0(X11;Linuxx86_64;rv:78.0)Gecko/20100101Firefox/78.0",
                "time": 1636382799,
                "mom_id": "ihmvVP/DGX7xyqKPr9wXRP582wyaqgQG8o7adFNQlqk59s6TGmhl/M9v2KY3OrzpuWA/z+LS5gS08gWHres09w=="
            },
            {
                "event": "tokeninfo_history",
                "ip": "142.42.42.42",
                "user_agent": "Mozilla/5.0(X11;Linuxx86_64;rv:78.0)Gecko/20100101Firefox/78.0",
                "time": 1636382799,
                "mom_id": "ihmvVP/DGX7xyqKPr9wXRP582wyaqgQG8o7adFNQlqk59s6TGmhl/M9v2KY3OrzpuWA/z+LS5gS08gWHres09w=="
            }
        ]
    }
    ```

## Subtokens

This operation requires the `tokeninfo:subtokens` capability.

### Subtokens Request
To query information about a mytoken's subtokens the client makes a subtokens request to the tokeninfo endpoint by adding the
following parameters using the `application/json` or `application/x-www-form-urlencoded` format in the HTTP request entity-body:

| Parameter | Necessity | Description         |
|-----------|-----------|---------------------|
| `action`  | REQUIRED  | MUST be `subtokens` |
| `mytoken` | REQUIRED  | The mytoken         |

!!! example
    ```http
    POST /api/v0/token/introspect HTTP/1.1
    Host: mytoken.example.com
    Content-Type: application/json

    {
        "action": "subtokens",
        "mytoken": "eyJhbGcio..."
    }
    ```

### Subtokens Response
A successful response returns the following parameters using the `application/json` media type:

| Parameter  | Necessity | Description                                                             |
|------------|-----------|-------------------------------------------------------------------------|
| `mytokens` | REQUIRED  | A `token` object holding information about the mytoken and its children |

The `token` object has the following attributes:

| Parameter  | Necessity | Description                                                 |
|------------|-----------|-------------------------------------------------------------|
| `token`    | REQUIRED  | A `token-data` object holding information about the mytoken |
| `children` | OPTIONAL  | A JSON Array of `token` objects - one for each subtoken     |

The `token-data` object has the following attributes:

| Parameter    | Necessity                 | Description                                                                                                                                                                                                  |
|--------------|---------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `name`       | OPTIONAL                  | The name of this mytoken                                                                                                                                                                                     |
| `mom_id`     | REQUIRED                  | A mom id (manage-other-mytokens ID) for this mytoken that can be used to manage it (revocation, event history); </br>this id is not the same as the JWT's `jti` and is not intended to be shown to the user. |
| `ip`         | REQUIRED                  | The IP that was used to create the mytoken                                                                                                                                                                   |
| `created`    | REQUIRED                  | A numeric time value (UNIX timestamp) indicating when this mytoken was created                                                                                                                               |
| `expires_at` | REQUIRED if token expires | A numeric time value (UNIX timestamp) indicating when this mytoken expires                                                                                                                                   |

!!! example
    ```http
    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "mytokens": {
            "token": {
                "ip": "127.0.0.1",
                "name": "mytoken-web",
                "mom_id": "abcdef",
                "created": 1636373529,
                "expires_at": 1636473529
            },
            "children": [
                {
                    "token": {
                        "ip": "127.0.0.1",
                        "name": "mytoken-web MT for list_mytokens",
                        "mom_id": "aabbcc",
                        "created": 1636384021
                        "expires_at": 1636473529
                    }
                },
                {
                    "token": {
                        "ip": "127.0.0.1",
                        "name": "mytoken-web MT for AT",
                        "mom_id": "ddeeff",
                        "created": 1636384026
                        "expires_at": 1636393529
                    }
                }
            ]
        }
    }
    ```

## List Mytokens

This action requires the `manage_mytokens:list` capability.

### List Mytokens Request

To list information about all mytokens of the user the client makes a list mytokens request to the tokeninfo endpoint by adding the
following parameters using the `application/json` or `application/x-www-form-urlencoded` format in the HTTP request entity-body:

| Parameter | Necessity | Description                |
|-----------|-----------|----------------------------|
| `action`  | REQUIRED  | MUST be `list_mytokens`    |
| `mytoken` | REQUIRED  | A mytoken as authorization |

!!! example
    ```http
    POST /api/v0/token/introspect HTTP/1.1
    Host: mytoken.example.com
    Content-Type: application/json

    {
        "action": "list_mytokens",
        "mytoken": "eyJhbGcio..."
    }
    ```

### List Mytokens Response
A successful response is very similar to a successful [Subtokens Response](#subtokens-response)
and returns the following parameters using the `application/json` media type:

| Parameter  | Necessity | Description                                                                                       |
|------------|-----------|---------------------------------------------------------------------------------------------------|
| `mytokens` | REQUIRED  | A JSON Array of `token` objects holding information about each of user's mytoken and its children |

The `token` object are described under [Subtokens Response](#subtokens-response)


!!! example
    ```http
    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "mytokens": [
            {
                "token": {
                    "ip": "127.0.0.1",
                    "name": "mytoken-web",
                    "mom_id": "fedcba",
                    "created": 1634311516,
                    "expires": 1634311517
                }
            },
            {
                "token": {
                    "ip": "127.0.0.1",
                    "name": "mytoken-web",
                    "mom_id": "abcdef",
                    "created": 1636373529,
                    "expires_at": 1636473529
                },
                "children": [
                    {
                        "token": {
                            "ip": "127.0.0.1",
                            "name": "mytoken-web MT for list_mytokens",
                            "mom_id": "aabbcc",
                            "created": 1636384021,
                            "expires_at": 1636473529
                        }
                    },
                    {
                        "token": {
                            "ip": "127.0.0.1",
                            "name": "mytoken-web MT for AT",
                            "mom_id": "ddeeff",
                            "created": 1636384026,
                            "expires_at": 1636393529
                        }
                    }
                ]
            }
        ]
    }
    ```
