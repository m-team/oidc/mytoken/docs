# Email Settings Endpoint
The email settings endpoint to obtain and update a user's email preferences.

## Get Email Info
### Email Info Request
To get information about a user's email preferences, the client sends a GET request to the email settings endpoint.
The request MUST include a mytoken with the `read@settings:email` capability as authorization.

!!! example
    ```http
    GET /api/v0/settings/email HTTP/1.1
    Host: mytoken.example.com
    Authorization: Bearer eyjsaiend...
    ```

### Email Info Response
A successful response returns the following parameters using the `application/json` media type:

| Parameter          | Necessity | Description                                                      |
|--------------------|-----------|------------------------------------------------------------------|
| `email_address`    | REQUIRED  | The user's email address                                         |
| `email_verified`   | REQUIRED  | A bool indicating if the user's mail was verified or not         |
| `prefer_html_mail` | REQUIRED  | A bool indicating if the user prefers html mails over plain text |

!!! example
    ```http
    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "email_address": "user@example.com",
        "email_verified": true,
        "prefer_html_mail": false
    }
    
    ```

## Update Email Preferences
### Update Email Preferences Request
To update a user's email preferences, the client sends a PUT request to the email settings endpoint and adds the
following parameters using the `application/json` or `application/x-www-form-urlencoded` format in the HTTP request entity-body:

| Parameter          | Necessity | Description                                                                     |
|--------------------|-----------|---------------------------------------------------------------------------------|
| `mytoken`          | REQUIRED  | A mytoken used as authorization; MUST have the `settings:email` capability      |
| `email_address`    | OPTIONAL  | The new email address                                                           |
| `prefer_html_mail` | OPTIONAL  | A bool indicating if the user wants to receive html mails instead of plain text |

Any number of request parameter can be given; all present ones will be updated to the according value. At least one 
request parameter (additionally to `mytoken`) MUST be given.

### Update Email Preferences Response
A successful response has the HTTP status code `204` and no content, unless the used mytoken has been rotated, in this
case the updated mytoken is returned with a status code of `200`.

!!! example
```http
PUT /api/v0/settings/email HTTP/1.1
Host: mytoken.example.com
Content-Type: application/json

    {
        "mytoken": "eyJhbGcio...",
        "email_address": "new.mail@example.com"
    }
    ```
