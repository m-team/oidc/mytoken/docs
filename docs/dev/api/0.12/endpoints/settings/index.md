# User Settings Endpoint
The user settings endpoint is an endpoint listing metadata about settings. In particular, it lists other setting related
endpoints, e.g. the [Grant Type Endpoint](grants/index.md).

## Settings Metadata Request
The client sends a GET request to the server's user settings endpoint.

!!! example
    ```http
    GET /api/v0/settings HTTP/1.1
    Host: mytoken.example.com
    ```

## Settings Metadata Response
The response uses the `application/json` media type.
The following metadata values are defined (all parameters that are applicable / defined for a server instance are required):

| Parameter             | Description                                                                                                                                |
|-----------------------|--------------------------------------------------------------------------------------------------------------------------------------------|
| `grant_type_endpoint` | Indicates where the Grant Type Endpoint can be found. Can be either a full absolute URL or an path relative to this settings endpoint.     |
| `email_endpoint`      | Indicates where the Email Settings Endpoint can be found. Can be either a full absolute URL or an path relative to this settings endpoint. |

!!! example
    ```http
    HTTP/1.1 200 OK
    Content-Type: application/json

    {
      "grant_type_endpoint": "https://mytoken.example.com/api/v0/settings/grants"
      "email_endpoint": "https://mytoken.example.com/api/v0/settings/email"
    }
    ```
