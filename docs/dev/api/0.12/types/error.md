# Error Response

In case of an error, an error response MUST be returned. The error response uses the `application/json` media type and
MUST contain the following fields:

| Claim | Description |
| ----- | ----------- |
| `error` | A short error name. |
| `error_description` | A longer error description. |

## Possible Errors
The `error` claim of an error response can be any error value defined by the OIDC and OAuth2 spec.
Additionally, we define the following values:

| `error` value | Description |
| ------------- | ----------- |
| `internal_server_error` | An internal server error |
| `oidc_error` | An OIDC related error |
| `insufficient_capabilities` | The used mytoken does not have the needed capabilities for the requested operation |
| `usage_restricted` | The used mytoken's restrictions do not allow the requested operation |
