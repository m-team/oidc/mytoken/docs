# Revocation Endpoint

The revocation endpoint is an endpoint to revoke Mytokens. All representations of mytokens can be revoked.

## Revocation Request
To revoke a mytoken the client makes a revocation request to the revocation endpoint by adding the
following parameters using the `application/json` or `application/x-www-form-urlencoded` format in the HTTP request entity-body:

| Parameter     | Necessity   | Description                                                                                                                       |
|---------------|-------------|-----------------------------------------------------------------------------------------------------------------------------------|
| `token`       | REQUIRED    | The token to be revoked. Can contain a JWT mytoken, short mytoken, or transfer code; the server automatically discovers the type. |
| `recursive`   | OPTIONAL    | Indicates if token should be revoked recursively. Default is `false`.                                                             |
| `oidc_issuer` | RECOMMENDED | The issuer URL of the OpenID Provider for which the token was issued.                                                             |

!!! example
    ```http
    POST /api/v0/token/revoke HTTP/1.1
    Host: mytoken.example.com
    Content-Type: application/json

    {
        "token": "eyJhbGcio...",
        "recursive": true,
        "oidc_issuer": "https://op.example.org"
    }
    ```

### The `recursive` Parameter
The `recursive` parameter indicates if the passed token should be revoked recursively. This means that also all
subtokens (mytokens, short_mytokens, and transfer_codes) are revoked recursively. 

## Revocation Response
On success, the server responses with the HTTP status code 204. If the client sent an invalid token, the server does not
send an error, since the presented token is invalid after the request.
The server does not send a response body.

!!! example
    ```http
    HTTP/1.1 204 OK
    ```

