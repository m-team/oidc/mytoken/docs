# Profiles and Templates Endpoint
The profiles and templates endpoint is an endpoint to obtain server-sided stored profiles and templates. In the 
following we refer to both as profiles.
These profiles can be referenced by their (full) name in any request where this is applicable. The content of a 
profile can also be obtained from this endpoint and directly included in a request as json.

This endpoint is more structured than other endpoints. In the following we refer to the endpoint uri as it is given 
in the [configuration endpoint](config.md) as the `base uri`.

Profiles can be grouped, i.e. different groups / organisations can have their own profiles. A full profile name is 
of the form `<group>/<profile_name>`. The default group for a mytoken server is named `_`.

## Obtaining Groups

### Groups Request
To query the list of groups on a mytoken server the client makes a `GET` request to the profiles endpoint base uri.

!!! example
    ```http
    GET /api/v0/pt HTTP/1.1
    Host: mytoken.example.com
    ```

### Groups Response
A successful response returns an array containing the group names using the `application/json` media type.

!!! example
    ```http
    HTTP/1.1 200 OK
    Content-Type: application/json

    [
        "_",
        "kit",
        "egi"
    ]
    ```

## Obtaining Profiles
Obtaining profiles and templates for the different types works exactly the same. There are just different endpoints:

| URI Path                          | Description                                     |
|-----------------------------------|-------------------------------------------------|
| `<base_uri>/<group>/profiles`     | Profiles for the `<group>` group.               |
| `<base_uri>/<group>/capabilities` | Capabilities Templates for the `<group>` group. |
| `<base_uri>/<group>/restrictions` | Restrictions Templates for the `<group>` group. |
| `<base_uri>/<group>/rotation`     | Rotation Templates for the `<group>` group.     |

### Profiles Request
To query the profiles for a group the client makes a `GET` request to the endpoint for that profile type for that 
group (see above).

!!! example
    ```http
    GET /api/v0/pt/_/capabilities HTTP/1.1
    Host: mytoken.example.com
    ```

### Profiles Response
A successful response returns an array of profile entries using the `application/json` media type.
Each profile entry has the following attributes (independent of profile type):

| Parameter    | Necessity | Description                     |
|--------------|-----------|---------------------------------|
| `id`         | REQUIRED  | A string identifying this entry |
| `name`       | REQUIRED  | The name of the profile         |
| `payload`    | REQUIRED  | The profile's payload as json   |

!!! example
    ```http
    HTTP/1.1 200 OK
    Content-Type: application/json

    [{
        "id": "6ebe1919-0b82-4940-8195-6ec4fca65b8c",
        "name": "all",
        "payload": [
            "AT",
            "tokeninfo",
            "manage_mytokens",
            "create_mytoken",
            "settings"
        ]
    }, {
        "id": "12a3516e-0d47-4a7b-b50d-0cb62778c4f9",
        "name": "default",
        "payload": [
            "AT",
            "tokeninfo"
        ]
    }]
    ```

## Managing Profiles
The administrator of a mytoken server can define the groups with their credentials and hand them out to others.
This allows organizations or other groups to create and manage their own profiles.

!!! important
    The create, update, and delete operations described below all require authentication by including the group 
    credentials with basic authentication in the request.

### Create a New Profile
#### Request
To create a new profile the client does a `POST` request to the profile types endpoint and includes the profile entry 
(omitting the id) in the request body using the `application/json` content type.


!!! example
    ```http
    POST /api/v0/pt/_/capabilities HTTP/1.1
    Host: mytoken.example.com
    Authorization: Basic XzphZG1pbgo=
    Content-Type: application/json

    {
        "name": "also-all",
        "payload": ["@_/all"]
    }
    ```
#### Response
On success the server responses with an http status code of `201` and an empty response body.

!!! example
    ```http
    HTTP/1.1 201 Created
    ```

### Update an Existing Profile
#### Request
To update a new profile the client sends a `PUT` request and includes the updated profile 
entry in the request body using the `application/json` content type.
All attributes of the updated profile must be given, not only the updated ones.

The request can be sent to either of the following endpoints:
- `<base_uri>/<group>/<profile_type>`
- `<base_uri>/<group>/<profile_type>/<id>`

In the latter case the `id` parameter in the profile entry can be omitted, in the first case it is required.

!!! example
    ```http
    POST /api/v0/pt/_/capabilities/12a3516e-0d47-4a7b-b50d-0cb62778c4f9 HTTP/1.1
    Host: mytoken.example.com
    Authorization: Basic XzphZG1pbgo=
    Content-Type: application/json

    {
        "name": "default",
        "payload": [
            "AT",
            "tokeninfo:introspect",
        ]
    }
    ```
#### Response
On success the server responses with an http status code of `200` and an empty response body.

!!! example
    ```http
    HTTP/1.1 200 OK
    ```

### Delete an Existing Profile
#### Request
To delete a new profile the client sends a `DELETE` request to either of the following endpoints:
- `<base_uri>/<group>/<profile_type>`
- `<base_uri>/<group>/<profile_type>/<id>`

In the first case the `id` parameter MUST be given in the request body.

!!! example
    ```http
    DELETE /api/v0/pt/_/capabilities/12a3516e-0d47-4a7b-b50d-0cb62778c4f9 HTTP/1.1
    Host: mytoken.example.com
    Authorization: Basic XzphZG1pbgo=
    ```
#### Response
On success the server responses with an http status code of `204` and an empty response body.

!!! example
    ```http
    HTTP/1.1 204 No Content
    ```
