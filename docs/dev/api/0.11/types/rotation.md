# Rotation

Token rotation can be enabled for mytokens. This means that when a mytoken is used, a new one is created and returned
in the response. The old mytoken can no longer be used. This means that each mytoken can only be used once, before it is rotated.

If a mytoken gets stolen,  both the legitimate user and the attacker will try to use the same mytoken. The mytoken
server is able to detect this and then can revoke this mytoken (recursively).

A Mytoken server SHOULD give the user the possibility to enable and disable token rotation for individual mytokens.
Token rotation is a useful security mechanism, but it is not suitable if a client cannot handle rotating mytokens or
multiple clients share a mytoken and they cannot synchronize the updated mytoken.

A `rotation` object has the following claims:

| Claim         | Necessity | Description                                                                                            |
|---------------|-----------|--------------------------------------------------------------------------------------------------------|
| `on_AT`       | OPTIONAL  | Indicates if the mytoken should be rotated on AT requests.                                             |
| `on_other`    | OPTIONAL  | Indicates if the mytoken should be rotated on requests other than AT.                                  |
| `lifetime`    | OPTIONAL  | Indicates the maximum lifetime of an individual mytoken. (See also [below](#about-the-lifetime-claim)) |
| `auto_revoke` | OPTIONAL  | Indicates if the mytoken chain is revoked when the mytoken server detects abuse.                       |

This `rotation` object can be used within mytoken requests to specify the rotation policy for the requested mytoken.
The same `rotation` object is used in the `rotation` claim of a JWT `mytoken` to describe the rotation policy that is in place for a mytoken (chain).

!!! important
    If a mytoken is used that has token rotation enabled, the response to any request can contain an updated Mytoken.
    To be more precise: If the mytoken was rotated (depending on the policy), the response MUST contain the updated mytoken.
    The updated mytoken is returned in the `updated_token` claim. This claim contains a whole mytoken response object as
    described in [the mytoken endpoint](../endpoints/mytoken.md#mytoken-response).

## About the `lifetime` Claim
The lifetime claim indicates the maximum lifetime of an individual mytoken. It cannot be used to exceed the lifetime of the whole
`mytoken chain` as it is defined by its `restrictions`.
However, renewable mytokens where each individual token has a limited lifetime are possible:

* A mytoken (chain) can have an absolute lifetime limit that is defined by its restrictions,
e.g. only valid until `2022-12-29`. There is no way to obtain a mytoken (from this chain) that will be valid beyond that date.
* Using the `lifetime` claim in the rotation object, it is possible to limit the lifetime of individual mytokens more.
!!! example
    - A Mytoken is created, and it is restricted so that it is only valid until `2022-12-29`.
    - Token rotation with a lifetime of one week is used. 
    
    Then the first mytoken `M1` will only be valid for one week,
    but if the token is being used within that week a new mytoken `M2` is created, which will again be valid for one week.
    This way it is possible to use this `mytoken chain` `M` until `2022-12-29`, but each individual mytoken `M*`
    can only be used for one week (and only once). If one mytoken in the chain is not used within one week,
    this specific mytoken and therefore also the whole chain expires and no other mytoken can be obtained from the chain
    (since it expired).
