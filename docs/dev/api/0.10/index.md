# Mytoken API <img src="https://img.shields.io/badge/-0.10-df691a" alt="0.10" style="height: 0.75em;"/>

Important types are described under [Types](types/index.md); this includes the 
[Mytoken type](types/mytoken.md).
The different endpoints and what they are used for are described under 
[Endpoints](endpoints/index.md).
