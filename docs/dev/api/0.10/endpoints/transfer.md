# Transfer Endpoint

The transfer endpoint is used to create a transfer code for an existing Mytoken.

## Mytoken Transfer Request
To transfer an existing mytoken the client makes a mytoken transfer request to the transfer endpoint by adding the
following parameters using the `application/json` or `application/x-www-form-urlencoded` format in the HTTP request entity-body:

| Parameter | Necessity | Description                            |
|-----------|-----------|----------------------------------------|
| `mytoken` | REQUIRED  | The mytoken that should be transferred |

!!! example
    ```http
    POST /api/v0/token/transfer HTTP/1.1
    Host: mytoken.example.com
    Content-Type: application/json

    {
        "mytoken": "eyJhbGcio..."
    }
    ```


## Mytoken Transfer Response
A successful response returns the following parameters using the `application/json` media type:

| Parameter       | Necessity | Description                                                                                     |
|-----------------|-----------|-------------------------------------------------------------------------------------------------|
| `transfer_code` | REQUIRED  | The created transfer code, that can be used at the [mytoken endpoint](mytoken.md#transfer-code) |
| `mytoken_type`  | OPTIONAL  | The type of the returned token; MUST be `transfer_code`                                         |
| `expires_in`    | REQUIRED  | The time in seconds in which the `transfer_code` expires                                        |

!!! example
    ```http
    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "transfer_code": "abcdefgh",
        "mytoken_type": "transfer_code",
        "expires_in": 300
    }
    ```

