# Grant Types Endpoint
The Grant Types Endpoint is an endpoint related to additional grant types that can be enabled and disabled by the user.

## Get Information About Grant Types
### Grant Type Info Request
To get information about a user's enabled grant types, the client sends a GET request to the grant type endpoint.
The request MUST include a mytoken with the `settings` capability as authorization.

!!! example
    ```http
    GET /api/v0/settings/grants HTTP/1.1
    Host: mytoken.example.com
    Authorization: Bearer eyjsaiend...
    ```

### Grant Type Info Response
A successful response returns the following parameters using the `application/json` media type:

| Parameter     | Necessity | Description                                                                            |
|---------------|-----------|----------------------------------------------------------------------------------------|
| `grant_types` | REQUIRED  | A JSON array of `grant_type` objects holding information about the enabled grant types |

The `grant_type` object has the following attributes:

| Parameter | Necessity | Description |
| --------- | --------- | ----------- |
| `grant_type` | REQUIRED | The name of the grant type |
| `enabled` | REQUIRED | A bool indicating if this grant is enabled or not |

!!! important
    The response MUST contain all enabled grant types, but disabled grant types MIGHT be omitted.

!!! example
    ```http
    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "grant_types": [
            {
                "grant_type": "ssh",
                "enabled": true
            }
        ]
    }
    ```

## Enable a Grant Type
### Enable Grant Type Request
To enable a grant type, the client sends a POST request to the grant type endpoint and adds the
following parameters using the `application/json` or `application/x-www-form-urlencoded` format in the HTTP request entity-body:

| Parameter | Necessity | Description |
| --------- | --------- | ----------- |
| `grant_type`  | REQUIRED | The grant type that should be enabled |
| `mytoken` | REQUIRED | A mytoken used as authorization; MUST have the `settings` capability |

!!! example
    ```http
    POST /api/v0/settings/grants HTTP/1.1
    Host: mytoken.example.com
    Content-Type: application/json

    {
        "grant_type": "ssh",
        "mytoken": "eyJhbGcio..."
    }
    ```

### Enable Grant Type Response
A successful response has the HTTP status code `201` and no content, unless the used mytoken has been rotated, in this
case the updated mytoken is returned with a status code of `200`.

## Disable a Grant Type
### Disable Grant Type Request
To disable a grant type, the client sends a DELETE request to the grant type endpoint and adds the
following parameters using the `application/json` or `application/x-www-form-urlencoded` format in the HTTP request entity-body:

| Parameter | Necessity | Description |
| --------- | --------- | ----------- |
| `grant_type`  | REQUIRED | The grant type that should be disabled |
| `mytoken` | REQUIRED | A mytoken used as authorization; MUST have the `settings` capability |

!!! example
    ```http
    DELETE /api/v0/settings/grants HTTP/1.1
    Host: mytoken.example.com
    Content-Type: application/json

    {
        "grant_type": "ssh",
        "mytoken": "eyJhbGcio..."
    }
    ```

### Disable Grant Type Response
A successful response has the HTTP status code `204` and no content, unless the used mytoken has been rotated, in this
case the updated mytoken is returned with a status code of `200`.
