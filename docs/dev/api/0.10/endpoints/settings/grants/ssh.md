# The SSH Grant Type

The `ssh` grant type is a grant type that is disabled by default, but can be enabled by the user. It allows users to
interact with the mytoken server through the `ssh` protocol.

## Listing Available SSH Keys

### SSH Keys Info Request
To get information about a user's enabled ssh keys, the client sends a GET request to the ssh settings endpoint.
The request MUST include a mytoken with the `settings` capability as authorization.

!!! example
    ```http
    GET /api/v0/settings/grants/ssh HTTP/1.1
    Host: mytoken.example.com
    Authorization: Bearer eyjsaiend...
    ```

### SSH Keys Info Response
A successful response returns the following parameters using the `application/json` media type:

| Parameter       | Necessity | Description                                            |
|-----------------|-----------|--------------------------------------------------------|
| `grant_enabled` | REQUIRED  | A bool indicating if the `ssh` grant is enabled or not |
| `ssh_keys`      | REQUIRED  | A JSON array of the enabled ssh keys                   |

The ssh keys are described by with the following attributes:

| Parameter    | Necessity                             | Description                                           |
|--------------|---------------------------------------|-------------------------------------------------------|
| `name`       | OPTIONAL                              | The name of the ssh key                               |
| `ssh_key_fp` | REQUIRED unless `ssh_key` is given    | The SHA256 fingerprint of the ssh key                 |
| `ssh_key`    | REQUIRED unless `ssh_key_fp` is given | The full ssh public key                               |
| `created`    | REQUIRED                              | The time when this ssh key was added                  |
| `last_used`  | OPTIONAL                              | The time when this ssh key was used for the last time |

!!! example
    ```http
    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "grant_enabled": true,
        "ssh_keys": [
            {
                "name": "example",
                "ssh_key_fp": "SHA256:7W4GUr6/Vkt+NgJEDxbWhV2BkmwuO9010iMGJPMNR8M",
                "created": 1638180570,
                "last_used": 1638182570
            }
        ]
    }
    ```

## Add an SSH Key
### Initial Add Request
To add a new ssh key, the client sends a request with the following parameters using the `application/json` or
`application/x-www-form-urlencoded` format to start the flow:

| Parameter               | Necessity   | Description                                                                                                                 |
|-------------------------|-------------|-----------------------------------------------------------------------------------------------------------------------------|
| `mytoken`               | REQUIRED    | A mytoken used as authorization; MUST have the `settings` capability                                                        |
| `grant_type`            | REQUIRED    | MUST be `mytoken`                                                                                                           |
| `ssh_key`               | REQUIRED    | The full ssh public key that should be added                                                                                |
| `name`                  | RECOMMENDED | A name for the ssh key                                                                                                      |
| `restrictions`          | OPTIONAL    | An array of Restrictions that are applied to the usage of the ssh key                                                       |
| `cpababilities`         | OPTIONAL    | An array of Capabilities that define what actions can be done with the shh key; if omitted only the `AT` capability is used |

### Add Response
The server answers with a response inline with the [flow to create a new mytoken with an OIDC flow](../../mytoken.md#authorization-response).
The further flow is also analogous to the OIDC flow to obtain a mytoken, however, the received polling code MUST be used
at this ssh settings endpoint and not at the mytoken endpoint. Apart from that the polling in analogous to the [polling
described for obtaining a mytoken](../../mytoken.md#polling-request).

!!! important
    The received polling code MUST be used at the ssh settings endpoint - not at the mytoken endpoint!

### Polling Response
The final response to the polling does not contain a mytoken but the information about the ssh connection.
A successful response returns the following parameters using the `application/json` media type:

| Parameter         | Necessity   | Description                                                      |
|-------------------|-------------|------------------------------------------------------------------|
| `ssh_user`        | REQUIRED    | The ssh username the user must use when using the ssh grant type |
| `ssh_host_config` | RECOMMENDED | A ssh host entry for the added ssh key                           |

The host entry a server SHOULD return in the `ssh_host_config` parameter should help users with the ssh integration.
They can paste it to their ssh config file and connect to the mytoken ssh server more easily.

!!! example
    ```http
    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "ssh_user": "fBIfLQhndkwiZHKJ",
        "ssh_host_config": "# Host entry for mytoken
    Host mytoken-example
        HostName mytoken.example.com
        Port 2222
        User fBIfLQhndkwiZHKJ
        # If you use a non-default ssh key for this entry, update the following line
        # IdentityFile ~/.ssh/your.key"
    }
    ```

## Remove an SSH Key
### Remove SSH Key Request
To remove an ssh key, the client sends a DELETE request to the ssh settings endpoint and adds the
following parameters using the `application/json` or `application/x-www-form-urlencoded` format in the HTTP request entity-body:

| Parameter    | Necessity                             | Description                                                          |
|--------------|---------------------------------------|----------------------------------------------------------------------|
| `mytoken`    | REQUIRED                              | A mytoken used as authorization; MUST have the `settings` capability |
| `ssh_key`    | REQUIRED unless `ssh_key_fp` is given | The full ssh public key that should be removed                       |
| `ssh_key_fp` | REQUIRED unless `ssh_key` is given    | The SHA256 fingerprint of the ssh public key that should be removed  |

!!! example
    ```http
    DELETE /api/v0/settings/grants/ssh HTTP/1.1
    Host: mytoken.example.com
    Content-Type: application/json

    {
        "mytoken": "eyJhbGcio...",
        "ssh_key_fp": "SHA256:7W4GUr6/Vkt+NgJEDxbWhV2BkmwuO9010iMGJPMNR8M"
    }
    ```

### Remove SSH Key Response
A successful response has the HTTP status code `204` and no content, unless the used mytoken has been rotated, in this
case the updated mytoken is returned with a status code of `200`.
