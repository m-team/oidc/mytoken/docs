# Mytoken

Mytokens are special tokens for the mytoken service. They are similar to OpenID Connect Refresh Tokens because they can
be used to obtain Access Token. Mytokens are Bearer Tokens and can be handed to other applications so that they can
easily obtain valid Access Tokens without any other mechanisms.
Mytokens cannot only be used to obtain access tokens, but other actions are possible. What can be done with a specific
Mytoken is defined by its [Capabilites](capabilities.md).

Mytokens can be restricted in several ways (see [Restrictions](restrictions.md)
), e.g. to be valid only for a limited
time, but a mytoken can also be unrestricted.

Mytokens can be obtained through different authentication mechanisms at the 
[Mytoken Endpoint](../endpoints/mytoken.md).

## The JWT Mytoken

Usually a mytoken is a signed JWT (note, that other representation as described below are possible). A mytoken has the following
claims:

| Claim          | Necessity                      | Description                                                                                                                                                                                                                                         |
|----------------|--------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `ver`          | REQUIRED                       | The token version this mytoken uses. This specification might change over time and therefore also the information encoded in a mytoken. The version helps applications to pass the mytoken. The current version is `0.5`.                           |
| `token_type`   | REQUIRED; MUST be `mytoken`.   | Indicates that this token is a mytoken. Useful if applications can be used with multiple tokens, e.g. OIDC Access Tokens and Mytokens. This claim can be used to distinguish them.                                                                  |
| `typ`          | REQUIRED; MUST be `Bearer`.    | Indicates that this is a bearer token.                                                                                                                                                                                                              |
| `iss`          | REQUIRED                       | URL using the https <br/><br/>scheme with no query or fragment component that the mytoken instance asserts as its Issuer Identifier. This MUST be identical to the issuer claim advertised at the [configuration endpoint](../endpoints/config.md). |
| `sub`          | REQUIRED                       | The subject of this token (i.e. an identifier for the subject user). It MUST uniquely identify the user. This specification does not mandate how the server creates the subject; one way is to combine the OIDC subject and issuer.                 |
| `exp`          | REQUIRED iff the token expires | The time when this token expires.                                                                                                                                                                                                                   |
| `nbf`          | REQUIRED                       | The time when the token can be used for the first time.                                                                                                                                                                                             |
| `iat`          | REQUIRED                       | The time when the token was issued.                                                                                                                                                                                                                 |
| `auth_time`    | RECOMMENDED                    | The time when the user performed the OIDC flow to obtain the linked OIDC token                                                                                                                                                                      |
| `jti`          | REQUIRED                       | A random and unique identifier.                                                                                                                                                                                                                     |
| `seq_no`       | REQUIRED                       | A sequence number for this mytoken. It is used for token rotation and therefore only relevant when token rotation is enabled.                                                                                                                       |
| `aud`          | REQUIRED                       | MUST be the server's issuer URL.                                                                                                                                                                                                                    |
| `oidc_sub`     | REQUIRED                       | The subject claim for this user at the given OpenID Provider.                                                                                                                                                                                       |
| `oidc_iss`     | REQUIRED                       | The issuer URL of the OpenID Provider for whom Access Tokens should be obtained.                                                                                                                                                                    |
| `restrictions` | OPTIONAL                       | An JSON Object defining the restrictions for this token. See [Restrictions](restrictions.md).                                                                                                                                                       |
| `capabilities` | REQUIRED                       | A JSON Array of capabilities for this token. See [Capabilities](capabilities.md) for details and a list of the possible values.                                                                                                                     |
| `rotation`     | OPTIONAL                       | A JSON Object describing the rotation policy for this mytoken. See [Rotation](rotation.md).                                                                                                                                                         |

!!! example
    ```json
    {
        "ver": "0.5",
        "token_type": "mytoken",
        "typ": "Bearer",
        "iss": "https://mytoken.example.com",
        "sub": "E0Cr0NTVuBVGJo5wvry4a63qM3Aedlgk+iseXTUEOPw=",
        "exp": 1672481471,
        "nbf": 1643763722,
        "iat": 1634116589,
        "auth_time": 1632112589,
        "jti": "50bb3a97-5480-4ff5-9373-25589bb8dab2",
        "seq_no": 1,
        "name": "Example",
        "aud": "https://mytoken.example.com",
        "oidc_sub": "example",
        "oidc_iss": "https://op.example.com",
        "restrictions": [
            {
                "nbf": 1643763722,
                "exp": 1672481471,
                "scope": "openid profile email eduperson_entitlement",
                "usages_AT": 1
            }
        ],
        "capabilities": [
            "create_mytoken",
            "tokeninfo_introspect",
            "tokeninfo_tree",
            "tokeninfo_history"
        ],
        "rotation": {
            "on_AT": true,
            "lifetime": 1000000,
            "auto_revoke": true
        }
    }
    ```

## Short Mytoken
A JWT mytoken can get quite long. For situations where a shorter token is required a **Short Mytoken** can be used. As
the name suggests a Short Mytoken is much shorter. While normal JWT mytokens are defined as a JWT, short mytokens are
opaque to clients.

A short mytoken can be used, created and revoked just like a JWT mytoken, in particular a short mytoken can be used
everywhere where a JWT mytoken can be used.

## Transfer Code
A transfer code is a short code that can be used to transfer a mytoken, e.g. to another machine.
A transfer code is only valid for a limited time (5min) and can only be used once. When the transfer code is used the
linked mytoken is returned and the transfer code is invalidated.

A transfer code can be created for an existing mytoken at the [transfer endpoint](../endpoints/transfer.md), but it is
also possible to directly obtain a transfer code from the [mytoken endpoint](../endpoints/mytoken.md) when creating a new
mytoken.

