# Types
The mytoken service introduces some types / objects that are not known within OAuth2 or OpenID Connect. The most
important one is the newly introduced [Mytoken](mytoken.md) token. In the 
following we describe all types
introduced with the mytoken service that are relavant for the API.

* [Mytoken (JWT)](mytoken.md)
* [Mytoken (Short)](mytoken.md#short-mytoken)
* [Mytoken (Transfer Code)](mytoken.md#transfer-code)
* [Restrictions](restrictions.md)
* [Capabilities](capabilities.md)
* [Rotation](rotation.md)
