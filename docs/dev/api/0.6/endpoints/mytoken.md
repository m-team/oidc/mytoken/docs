# Mytoken Endpoint

The mytoken endpoint is one of the most important endpoints of the mytoken server.
It is also the most versatile endpoint, offering different ways to obtain new mytokens.

These "ways" are called `grant types` - types how a mytoken can be granted.
The two core grant types are the [`mytoken`](#existing-mytoken) grant type and the [`oidc_flow`](#oidc-flow) one, but there are also others.
In the following we describe how a mytoken can be obtained from the mytoken endpoint with each of the different grants.


## Existing Mytoken
An existing mytoken can be used with the `mytoken` grant type to create a new (sub-)token. This requires a mytoken with
the `create_mytoken` capability.

### Mytoken Request - Mytoken Grant
To create a new (sub-)mytoken from an existing mytoken the client makes a mytoken request to the mytoken endpoint by adding the
following parameters using the `application/json` or `application/x-www-form-urlencoded` format in the HTTP request entity-body:

| Parameter               | Necessity                                             | Description                                                                                                                                                                         |
|-------------------------|-------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `oidc_issuer`           | OPTIONAL                                              | The issuer url of the OpenID provider                                                                                                                                               |
| `grant_type`            | REQUIRED                                              | MUST be `mytoken`                                                                                                                                                                   |
| `mytoken`               | REQUIRED                                              | The mytoken used for authentication                                                                                                                                                 |
| `restrictions`          | OPTIONAL                                              | A JSON Object defining the restrictions for <br/><br/>the new token as defined under [Types](../types/restrictions.md). MUST be a superset of the restrictions of the used mytoken. |
| `error_on_restrictions` | OPTIONAL                                              | A bool indicating the behavior if the requested restrictions are not a superset of the original token.                                                                              |
| `capabilities`          | OPTIONAL                                              | A JSON Array of capabilities for the new token. MUST be a subset of the ‘subtoken_capabilities’ of the used Mytoken.                                                                |
| `subtoken_capabilities` | OPTIONAL                                              | A JSON Array of capabilities. The new mytoken can only create mytokens with these capabilities. MUST be a subset of the ‘subtoken_capabilities’ of the used Mytoken.                |
| `name`                  | OPTIONAL                                              | A name identifying / describing the new mytoken given by the user / client                                                                                                          |
| `rotation`              | OPTIONAL                                              | A rotation object as defined under [Types](../types/rotation.md) describing the rotation policy for the new mytoken                                                                 |
| `response_type`         | OPTIONAL; MUST not be used if `max_token_len` is used | Determines the representation of the returned mytoken. Default is `token` (JWT); possible values are `token`, `short_token`, `transfer_code`                                        |
| `max_token_len`         | OPTIONAL; MUST not be used if `response_type` is used | Numeric value indicating the maximum length the returned token is allowed to have; the server uses the parameter to determine the representation of the returned mytoken            |

!!! example
    ```http
    POST /api/v0/token/my HTTP/1.1
    Host: mytoken.example.com
    Content-Type: application/json

    {
        "grant_type": "mytoken",
        "mytoken": "eyJhbGcio...",
        "restrictions": [
            {
                "exp": 19829349983,
                "scope": "openid profile"
            }
        ],
        "capabilities": ["AT"],
        "name": "example mytoken",
        "max_token_len": 512
    }
    ```

### Mytoken Response - Mytoken Grant
A successful response returns a mytoken as defined under [Mytoken Response](#mytoken-response).

## OIDC Flow
A mytoken can be created from an OIDC authorization flow. The user then authenticates against the OpenID provider and no
other prerequisites (such as an existing mytoken) are required.
Since this is the only grant type that enables the server to obtain an OIDC refresh token, this flow has to be done by
each user at least once, i.e. the first mytoken has to obtained through this flow.

The mytoken server only supports the authorization code flow. While the server could be extended with additional OIDC flows,
the authorization code flow is the one offering the best adaption by providers and also fully satisfies user needs.
In particular the device flow is not needed between the mytoken server and the OpenID provider, because the server
offers a similar experience between the client and the mytoken server.

Obtaining a mytoken through the `oidc_flow` grant type requires multiple requests by the client and goes beyond a single
request-response.

### Authorization Request
To start the flow the client makes a request to the mytoken endpoint by adding the
following parameters using the `application/json` or `application/x-www-form-urlencoded` format in the HTTP request entity-body:

| Parameter               | Necessity                                             | Description                                                                                                                                                              |
|-------------------------|-------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `oidc_issuer`           | REQUIRED                                              | The issuer url of the OpenID provider                                                                                                                                    |
| `grant_type`            | REQUIRED                                              | MUST be `oidc_flow`                                                                                                                                                      |
| `oidc_flow`             | REQUIRED                                              | MUST be `authorization_code`                                                                                                                                             |
| `restrictions`          | OPTIONAL                                              | A JSON Object defining the restrictions for <br/>the new token as defined under [Types](../types/restrictions.md). If omitted no restrictions are applied.               |
| `capabilities`          | OPTIONAL                                              | A JSON Array of capabilities for the new token. If omitted only the `AT` capability is used.                                                                             |
| `subtoken_capabilities` | OPTIONAL                                              | A JSON Array of capabilities. The new mytoken can only create mytokens with these capabilities. If omitted equal to `capabilities`.                                      |
| `client_type`           | OPTIONAL                                              | Either `web` or `native`; default is `native`                                                                                                                            |
| `redirect_uri`          | REQUIRED iff `client_type` is `web`                   | The url to which the server redirects after the flow                                                                                                                     |
| `name`                  | OPTIONAL                                              | A name identifying / describing the new mytoken given by the user / client                                                                                               |
| `rotation`              | OPTIONAL                                              | A rotation object as defined under [Types](../types/rotation.md) describing the rotation policy for the new mytoken                                                      |
| `response_type`         | OPTIONAL; MUST not be used if `max_token_len` is used | Determines the representation of the returned mytoken. Default is `token` (JWT); possible values are `token`, `short_token`, `transfer_code`                             |
| `max_token_len`         | OPTIONAL; MUST not be used if `response_type` is used | Numeric value indicating the maximum length the returned token is allowed to have; the server uses the parameter to determine the representation of the returned mytoken |

!!! example
    ```http
    POST /api/v0/token/my HTTP/1.1
    Host: mytoken.example.com
    Content-Type: application/json

    {
        "grant_type": "oidc_flow",
        "oidc_flow": "authorization_code",
        "oidc_issuer": "https://op.example.org",
        "restrictions": [
            {
                "exp": 19829349983,
                "scope": "openid profile"
            }
        ],
        "capabilities": ["AT"],
        "name": "example mytoken",
        "max_token_len": 512
    }
    ```

    
### Authorization Response
A successful response returns the following parameters using the `application/json` media type:

| Parameter     | Necessity | Description                                                            |
|---------------|-----------|------------------------------------------------------------------------|
| `consent_uri` | REQUIRED  | The generated authorization url the client should redirect the user to |

If the `client_type` was `native` the response can contain the following additional parameters:

| Parameter      | Necessity | Description                                                                                                                      |
|----------------|-----------|----------------------------------------------------------------------------------------------------------------------------------|
| `polling_code` | REQUIRED  | A code the client can use to poll for the mytoken; see [polling code grant](#polling-code)                                       |
| `expires_in`   | REQUIRED  | The lifetime of the polling code given in seconds                                                                                |
| `interval`     | OPTIONAL  | The interval given in seconds in which the client should poll; if omitted the client SHOULD use an polling interval of 5 seconds |

!!! example
    ```http
    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "consent_uri": "https://mytoken.example.com/c/hdkwls",
        "polling_code": "abcdefgh",
        "expires_in": 300
    }
    ```

The client MUST redirect the user to the obtained authorization url or show it to the user so that they can open it.
Upon opening the authorization url the user is presented a consent screen where they can approve or decline the creation
of the mytoken. Users can also make changes to the properties of the mytoken in the consent screen.
After acceptance, the user is redirected to the OpenID provider where they have to authenticate. At the end of the
authorization code flow the mytoken server obtains an OIDC refresh token and creates the requested mytoken.

In the `web` flow the mytoken server redirects to the specified redirection url and sets the mytoken in the `mytoken`
cookie.
In the `native` flow the mytoken servers redirects to a page informing the user about the success. The user then should
go back to the native application.
In between the client polls the mytoken endpoint with the received polling code as described [below](#polling-code).

## Polling Code

Native applications when using the authorization code flow poll the mytoken endpoint to receive the mytoken in the end.

### Polling Request
A polling request includes the
following parameters using the `application/json` or `application/x-www-form-urlencoded` format in the HTTP request entity-body:

| Parameter | Necessity | Description |
| --------- | --------- | ----------- |
| `grant_type` | REQUIRED | MUST be `polling_code` |
| `polling_code` | REQUIRED | The polling code |

!!! example
    ```http
    POST /api/v0/token/my HTTP/1.1
    Host: mytoken.example.com
    Content-Type: application/json

    {
        "grant_type": "polling_code",
        "polling_code": "ghijklmn"
    }
    ```

### Polling Response
If the user has approved the grant, the mytoken endpoint responds with a successful response as defined under
[Mytoken Response](#mytoken-response); otherwise, it responds with an error, as defined in
[Section 3.5 of RFC 8628](https://tools.ietf.org/html/rfc8628#section-3.5).


## Transfer Code

A transfer code can be used to exchange the code into the mytoken.

### Transfer Request
A transfer request includes the
following parameters using the `application/json` or `application/x-www-form-urlencoded` format in the HTTP request entity-body:

| Parameter | Necessity | Description |
| --------- | --------- | ----------- |
| `grant_type` | REQUIRED | MUST be `transfer_code` |
| `transfer_code` | REQUIRED | The transfer code |

!!! example
    ```http
    POST /api/v0/token/my HTTP/1.1
    Host: mytoken.example.com
    Content-Type: application/json

    {
        "grant_type": "transfer_code",
        "transfer_code": "ghijklmn"
    }
    ```

### Transfer Response
A successful response returns a mytoken as defined under [Mytoken Response](#mytoken-response).


## Mytoken Response
A successful mytoken response returns the following parameters using the `application/json` media type:

| Parameter               | Necessity                                                                          | Description                                                                                                                                      |
|-------------------------|------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------|
| `mytoken`               | REQUIRED if `mytoken_type` is `token` or `short_token`; Otherwise MUST NOT be used | The issued mytoken                                                                                                                               |
| `mytoken_type`          | REQUIRED if a short mytoken is returned; otherwise RECOMMENDED                     | The representation type of the returned mytoken. Can be any of the possible values of the `response_type` request parameter. Default is `token`. |
| `transfer_code`         | REQUIRED if `mytoken_type` is `transfer_code`; otherwise MUST NOT be used          | The issued transfer code                                                                                                                         |
| `expires_in`            | RECOMMENDED                                                                        | The number of seconds in which the returned mytoken or transfer code expires.                                                                    |
| `restrictions`          | OPTIONAL                                                                           | The [restrictions object](../types/restrictions.md) for the returned mytoken                                                                     |
| `capabilities`          | OPTIONAL                                                                           | A JSON Array describing the capabilities of the returned mytoken                                                                                 |
| `subtoken_capabilities` | OPTIONAL                                                                           | A JSON Array describing the capabilities that a mytoken created from the returned mytoken might have                                             |
| `rotation`              | OPTIONAL                                                                           | The [rotation object](../types/rotation.md) for the returned mytoken                                                                             |

!!! example "Mytoken Example Response"
    ```http
    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "mytoken": "aBcDeFgH...",
        "mytoken_type": "short_token",
        "expires_in": 1000000,
        "capabilities": ["AT"]
    }
    ```
    
!!! example "Transfer Code Example Response"
    ```http
    HTTP/1.1 200 OK
    Content-Type: application/json

    {
        "transfer_code": "abcdefgh",
        "mytoken_type": "transfer_code",
        "expires_in": 300
    }
    ```
