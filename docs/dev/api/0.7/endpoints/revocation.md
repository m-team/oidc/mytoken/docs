# Revocation Endpoint

The revocation endpoint is an endpoint to revoke Mytokens. All representations of mytokens can be revoked.

## Revocation Request
To revoke a mytoken the client makes a revocation request to the revocation endpoint by adding the
following parameters using the `application/json` or `application/x-www-form-urlencoded` format in the HTTP request entity-body:

| Parameter       | Necessity   | Description                                                                                                                                                                    |
|-----------------|-------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `token`         | REQUIRED    | The token to be revoked. Can contain a JWT mytoken, short mytoken, or transfer code; the server automatically discovers the type.                                              |
| `revocation_id` | OPTIONAL    | If given the mytoken associated with this revocation id is revoked, not the token in the `token` parameter, the `token` parameter is still required and used as authorization. |
| `recursive`     | OPTIONAL    | Indicates if token should be revoked recursively. Default is `false`.                                                                                                          |
| `oidc_issuer`   | RECOMMENDED | The issuer URL of the OpenID Provider for which the token was issued.                                                                                                          |

!!! example
    ```http
    POST /api/v0/token/revoke HTTP/1.1
    Host: mytoken.example.com
    Content-Type: application/json

    {
        "token": "eyJhbGcio...",
        "recursive": true,
        "oidc_issuer": "https://op.example.org"
    }
    ```

### The `recursive` Parameter
The `recursive` parameter indicates if the passed token should be revoked recursively. This means that also all
subtokens (mytokens, short_mytokens, and transfer_codes) are revoked recursively. 

### The `revocation_id` Parameter
If given, the mytoken associated with `revocation_id` is revoked instead of the token in the `token` parameter. The 
`token` parameter is still required since it is used for authorization. This token can only revoke another token if it
has the `revoke_any_token` capability or if the token to be revoked is a subtoken.

!!! important
    The `revocation_id` of a token is not the same as the `jti` of the JWT. The `revocation_id` of a token can be 
    obtained from a tokeninfo [`subtokens`](tokeninfo.md#subtokens) or [`list mytokens`](tokeninfo.md#list-mytokens)
    request.

## Revocation Response
On success, the server responses with the HTTP status code 204. If the client sent an invalid token, the server does not
send an error, since the presented token is invalid after the request.
The server does not send a response body.

!!! example
    ```http
    HTTP/1.1 204 OK
    ```

