# Build mytoken From Source
Building mytoken from source is more complicated than installing it from a packaged release. In the following we describe the necessary steps.

## Install golang
Please refer to the [golang documentation](https://golang.org/doc/install) on how to install go on your system.
Be aware that your distribution might provide a golang package for easier installation. However, when installing from
your distribution's package assert that the provided version is recent enough.

!!! important "Version"
    Mytoken requires at least golang 1.19

## Build and install binaries
The mytoken server comes as a single binary. However, there is also a `mytoken-setup` and `mytoken-migratedb` binary that help with important
setups.

To build and install all binaries run the following commands:

    GO111MODULE=on go get github.com/oidc-mytoken/server/cmd/mytoken-server
    GO111MODULE=on go get github.com/oidc-mytoken/server/cmd/mytoken-server/mytoken-notifier-server
    GO111MODULE=on go get github.com/oidc-mytoken/server/cmd/mytoken-server/mytoken-setup
    GO111MODULE=on go get github.com/oidc-mytoken/server/cmd/mytoken-server/mytoken-migratedb

The binaries will be installed into `$GOPATH/bin/`.

!!! note
    If you want to make changes to the code or build without installing the binaries, you have to get the source code, e.g. with 
    ```
    git clone https://github.com/oidc-mytoken/server
    ```
    To build all binaries run the following in the git root directory:
    ```
    GO111MODULE=on go build github.com/oidc-mytoken/server/cmd/mytoken-server
    GO111MODULE=on go build github.com/oidc-mytoken/server/cmd/mytoken-server/mytoken-notifier-server
    GO111MODULE=on go build github.com/oidc-mytoken/server/cmd/mytoken-server/mytoken-setup
    GO111MODULE=on go build github.com/oidc-mytoken/server/cmd/mytoken-server/mytoken-migratedb
    ```
    This will place the binaries in your current working directory.
    
## Create the relevant directories
The following directories are needed by mytoken and normally created by the distribution's package.
You now have to create these manually.

Create the directory for the configuration file:

    mkdir /etc/mytoken
    
Create a directory where the log files are stored:

    mkdir /var/log/mytoken

## Copy the example configuration
Copy the example configuration file to the created configuration directory:

    cp config/example-config.yaml /etc/mytoken/

## Create a service file
Create a file called `/etc/systemd/system/mytoken.service` with the following content:

    [Unit]
    Description=mytoken
    After=network.target auditd.service
    
    [Service]
    ExecStart=/root/go/bin/mytoken-server
    ExecReload=/bin/kill -HUP $MAINPID
    WorkingDirectory=/root/mytoken
    KillMode=process
    Restart=always
    RestartPreventExitStatus=255
    Type=simple
    
    [Install]
    WantedBy=multi-user.target

`ExecStart` should point to your `mytoken-server` binary.

## Setup and Configuration
For the remaining configuration please refer to the [Deployment Guide starting at section 'Configuration'](index.md#configuration).