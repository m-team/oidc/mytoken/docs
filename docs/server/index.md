# Deployment Guide

Mytoken is provided under the [MIT license](https://github.com/oidc-mytoken/server/blob/master/LICENSE), can be  freely
adapted, and everyone is free to run their own instance.

We invested some time and effort into reducing the complexity of a deployment, making it as easy as possible.

Mytoken can be deployed natively or in docker containers. Information on how mytoken can be deployed with the help 
of `docker swarm` can be found [here](docker.md).
In the following we describe the necessary steps to deploy mytoken natively.

## Install mytoken-server
By far the easiest way to install `mytoken-server` is to use a packaged 
release. However, you can also [build mytoken yourself](build.md).

You can download a packaged release version for your distribution from the [release page](https://github.com/oidc-mytoken/server/releases).
We also provide packages for various distributions at  [http://repo.data.kit.edu/](http://repo.data.
kit.edu/).

The mytoken server project provides a number of different packages:

- `mytoken-server`: This is the actual mytoken server
- `mytoken-notifier-server`: This is a standalone notifier server used to 
  send notifications. It is only needed if the notifications feature is 
  enabled and you don't want to use the notifier integrated into 
  `mytoken-server`, i.e. in a distributed deployment.
- `mytoken-server-migratedb`: This is a tool that helps to prepare and migrate the database between version upgrades.
- `mytoken-server-setup`: This is a tool that helps with different setups, e.g. generating keys.

## Configuration
Now you can start configuring your mytoken instance.
Please start by copying the example configuration file.

    cp /etc/mytoken/example-config.yaml /etc/mytoken/config.yaml

Then start editing the configuration file `/etc/mytoken/config.yaml`.
The example configuration file contains comments that give short explanations for the configuration options.
In the following we will give more information on some options:

- `server.tls.cert` and `server.tls.key` take an absolute path to the tls certificate and key file. The cert file must contain the whole cert chain.
- `geo_ip_db_file` takes an absolute path. The file will be installed later by [setup](#run-setup) to that location.
- `database.user` does not have to exist in the database. It will be created later by [setup](#run-setup).
- `database.db` does not have to exist in the database. It will be created later by [setup](#run-setup).
- `signing.key_file` takes an absolute path. The key file does not have to exist, it will be created later by [setup](#run-setup) at that location.
- `features` can be used to enable/disable certain features and set some properties.
- `providers`: Here you can add all the OpenID providers you want to support with your mytoken instance. You can support more than one provider.

## Setup logrotate
Mytoken will log all http requests to a single `access.log` file and all other log messages to a single `mytoken.log` file (if log to file is configured).
To rotate log files the [logrotate utility](https://linux.die.net/man/8/logrotate) can be used.

The following logrotate configuration can be used:

    /var/log/mytoken/*.log {
        daily
        missingok
        rotate 365
        maxage 365
        compress
        delaycompress
        notifempty
        create 600 root root
        dateext
        sharedscripts
        postrotate
                    if invoke-rc.d mytoken status > /dev/null 2>&1; then 
                        PID=$(/usr/bin/systemctl show --property MainPID --value mytoken 2>/dev/null)
                        /bin/kill -USR1 $PID  > /dev/null 2>&1
                    fi;
        endscript
    }
    
The file should be placed in `/etc/logrotate.d/mytoken`.

!!! Note
    You can adapt the settings to your needs.
    
## Register OIDC clients
If not already done, register a client for each provider you want to support.
If you want to run your own instance of mytoken, we assume you are familiar with registering OIDC clients.
Note that the redirect url must be of the form `<issuer>/redirect` (where `<issuer>` is the issuer url of your mytoken instance).
Then add the client to the configuration file under `providers`.

## Run setup
The `mytoken-setup` binary eases the setup of various things that are still missing.

!!! note
    `mytoken-setup` reads your configuration file and prepares things as configured in that file. You should have
    finished the configuration of your mytoken instance now.

### Create a signing key
You must create a signing key in order to sign mytoken tokens:

    mytoken-setup signing-key
    
This will create a new signing key according to the properties defined in the configuration file and store it to the correct location.

### Install GeoIP-Location Database

    mytoken-setup install geoip-db
  
will download and copy the geoip location database to the correct location.

## Generate `ssh` keys
If you enabled the `ssh` grant type feature you must provide ssh host keys 
in configuration file.
You MUST use ssh keys different from the host's native `sshd` keys.

To generate a new set of host keys run the following:
```shell
mkdir ssh
ssh-keygen -t rsa -b 4096 -f ssh/ssh_host_rsa_key -N ""
ssh-keygen -t ed25519 -f ssh/ssh_host_ed25519_key -N ""
ssh-keygen -t ecdsa -b 521 -f ssh/ssh_host_ecdsa_key -N ""
```

## Prepare the database
In this step we will prepare the database, more particularly all needed tables are created.

Mytoken uses `mariadb` as its database (at least version `10.5.2` is required). Refer to 
[their documentation](https://mariadb.com/kb/en/where-to-download-mariadb/)
on how to install mariadb on your system. (Mytoken also supports the usage of a [galera cluster](https://mariadb.com/kb/en/getting-started-with-mariadb-galera-cluster/).
Simply specify all hosts in the mytoken configuration file.)

`mytoken-migratedb` will do most of the work for us, but it needs root access to the database. Depending on the configuration
of your mariadb server you might need to set a root password first. You also must create a database for mytoken (named as specified in your configuration file).
We can use the `mytoken-setup` tool for this first steps:

    mytoken-setup db db --pw-file=<path/to/a/file/with/root/password>
    mytoken-setup db user --pw-file=<path/to/a/file/with/root/password>

With a password authenticate-able root user and the database ready we can setup all the database tables and other data:

    mytoken-migratedb -f
   
## Test the installation
Startup the server to test everything is fine:

    mytoken-server
    
## Enable the mytoken service
Mytoken installed a `systemd service` file. 
To enable the service, run:

    systemctl enable mytoken

Now you can start, stop, restart, and reload mytoken:

    service mytoken start
    service mytoken stop
    service mytoken restart
    service mytoken reload