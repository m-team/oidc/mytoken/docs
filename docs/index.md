# Mytoken
![logo](img/mytoken.png)

!!! abstract "Mytoken"
    `Mytoken` is a web service to obtain OpenID Connect Access Tokens in an easy but secure way for extended periods of time
    and across multiple devices. In particular, `mytoken` was developed to provide OIDC Access Tokens to long-running compute
    jobs.

Access Tokens can be obtained via so-called **`mytokens`**: A new token type that can be easily used as a Bearer token from
any device. These `mytokens` can be restricted according to the particular use case to only allow the needed privileges.

`Mytoken` focuses on integration with the command line through our [command line client](client/index.md) <!-- and [oidc-agent](https://github.com/indigo-dc/oidc-agent) --> but also offers a web interface. A demo instance is available at  [https://mytoken.data.kit.edu/](https://mytoken.data.kit.edu/).

To get started you can use the following resources:

- [Getting started (command line)](start/cmd.md)
- [Getting started (web)](start/web.md)
- [More information about the `mytoken` token and other concepts](concepts/index.md)
- [Technical details](dev/technical/index.md)
- [Frequently asked questions](faq/index.md)

