# Getting Started
To get started it is a good idea to get familiar with the [mytoken concepts](../concepts/index.md).
But you can also skip this and go back to the concepts later.

You can get started with mytoken on the [command line](cmd.md) or the [web](web.md).

We also have a guide to get started with the [`ssh` grant type](ssh.md).
