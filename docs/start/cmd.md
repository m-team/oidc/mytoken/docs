---
title: Command Line
---

## Command Line - Usage options
To use mytokens on the command line different options are available. The two common ones are the mytoken command 
line client and oidc-agent. You can also interact directly with the API via e.g. `curl`, but this is more complex.
However, for environments where no additional software can be installed the following one-line frankenstein command 
might be useful to obtain an access token:
```bash
curl -X POST \
  $(curl \
    $(echo $MYTOKEN | (cut -d '.' -f2; echo ====;) | tr -d '\n' | fold -w 4 | sed '$ d' | tr -d '\n' | \
    base64 -d  | \
    jq -r .iss | sed 's:/*$::' \
    )/.well-known/mytoken-configuration 2>/dev/null | \
  jq -r .access_token_endpoint \
  ) \
  -H "Content-Type: application/json" \
  -d "{\"grant_type\":\"mytoken\",\"mytoken\":\"$MYTOKEN\"}" 2>/dev/null | \
  jq -r .access_token
```

### Command Line Client
!!! summary
    The mytoken command line client is the client tool of choice if you already have a mytoken and want to use it, 
    but do not have to take care of securely storing the mytoken.

The command line client can be used to perform various actions that can be done with a mytoken - most importantly 
obtaining access tokens.
However, it does not manage your mytokens. This means that you can only use it with an already existing mytoken or 
you can use it to create a new mytoken from an OIDC flow. But you have to take care of the mytoken yourself - this 
includes **secure** storage of the mytoken.
If you don't want to do that use `oidc-agent`.

See [below](#command-line-client-quickstart) for the quickstart.

### oidc-agent
!!! summary
    `oidc-agent` can save a "master" mytoken securely in an agent account configuration. You can use it to obtain 
    access tokens or (sub-)mytokens.

The [`oidc-agent`](https://github.com/indigo-dc/oidc-agent) tool has support for mytoken integrated (starting at 
version `4.4.0`). The agent stores a master mytoken securely, and you can use it to obtain access tokens. If you 
already know `oidc-agent` this will feel exactly like a normal OIDC account configuration, with the only difference 
that you and other applications additionally can request mytokens. If a mytoken is requested you always have to 
confirm the request (and can make changes to the properties of the requested mytoken).

See [below](#oidc-agent-quickstart) for the quickstart.

## Command Line Client - Quickstart
To use a mytoken for any action you have to pass it to the client. There are several options available:
- `--MT-file`
- `--MT-env`
- `--MT-prompt`
- `--MT`
- [WLCG Bearer Token Discovery](https://github.com/WLCG-AuthZ-WG/bearer-token-discovery)

If no mytoken is passed and the `MT` command is used a new mytoken is created from an OIDC flow. (This can also be 
done by passing the `--oidc` option explicitly.)

We recommend passing a mytoken via a file. This way the client can update it, in the case that it changes (i.e. 
token rotation).

In the following we assume that a mytoken is passed in any way and leave these options out of the commands.

### Obtaining an Access Token
```shell
mytoken AT
```
This obtains an access token with the default (maximum) scopes and audiences. To request specific scopes / audiences 
use the `--scope` and `--aud` options, respectively.

### Obtaining a Mytoken
```shell
mytoken MT
```
This obtains a sub-mytoken with the same restrictions and capabilities as the original mytoken.
To request specific properties use the `--capabilities`, `--restrictions`, or [`--profile`](../concepts/profiles.md) 
options.

### More
For more information please refer to the [detailed client documentation](../client/index.md).

## OIDC-agent - Quickstart
### New Mytoken Account Configuration
To create a new mytoken-based agent account configuration run `oidc-gen` with the `--mytoken-url` option. You can 
also pass a [mytoken profile](../concepts/profiles.md) to the `--mytoken-profile` option.

```shell
oidc-gen --mytoken-url="https://mytoken.data.kit.edu" --mytoken-profile=agent <shortname>
```

This will start an OIDC flow and eventually create the account configuration.

### Obtaining Access Tokens
Obtaining access tokens with a mytoken-based account configuration works exactly as with original OIDC account 
configurations.

```shell
oidc-token <shortname>
```
also 
```shell
oidc-token <OP_issuer_url>
```
will work as expected.

To request specific scopes / audiences use the `--scope`/`-s` or `--aud` options, respectively.

### Obtaining Mytokens
To obtain a mytoken through `oidc-token` include the `--MT` option and optionally pass a 
[mytoken profile](../concepts/profiles.md) to request specific properties.

```shell
oidc-token <shortname> --MT[=profile]
```

Other applications can also request mytokens through the [agent's API](https://indigo-dc.gitbook.io/oidc-agent/api).

In all cases the user has to approve the mytoken and then can also make changes to the mytoken's properties.

### More
For more information about the oidc-agent and how to use it (with and without mytokens) please refer to [its 
documentation](https://indigo-dc.gitbook.io/oidc-agent).