---
title: SSH
---

# The `ssh` Grant Type

The `ssh` grant type is a user grant type. This means it must be enabled by a user before it can be used. This also 
requires additional setup (an ssh key must be added). The `ssh` grant type allows to perform actions that would 
normally require a mytoken (e.g. obtain a mytoken or access token) through the ssh protocol.
This allows users to obtain access tokens (and others) without having to store a mytoken, but only an ssh key which 
they might already have.

## Enabling the `ssh` Grant
The ssh grant type is disabled on default, users have to enable it manually.

This can be done at the web interface or with the command line client:

!!! web "Webinterface"
    - In the upper right corner click on 'settings' (the gear icon).
    - Enable the `ssh` grant by switching the toggle into the 'on' position.

!!! client "Command Line Client"
    Enabling the `ssh` grant requires a mytoken with the `settings:grants` capability (which is included in the 
    `settings` capability). The following command line can be used:
    ```
    mytoken settings grants enable ssh
    ```

## Add an SSH Key
To use the `ssh` grant a user first has to upload a public key. As part of the flow to add an ssh key, a new OpenID 
Connect authorization flow has to be done.
Users can also define which actions can be done through ssh (capabilities) and restrict the usage (restrictions).

!!! web "Webinterface"
    On the ssh user grant page a new ssh key can be added. The public key can be uploaded from a file or the content 
    can be pasted. It is also possible to set capabilities and restrictions.

!!! client "Command Line Client"
    Use the following command:
    ```
    mytoken settings grants ssh add --key-name <key_name> <path/to/public/key>
    ```
    It is also possible to set capabilities and restrictions.

The server returns the username that must be used with the ssh protocol. The server also returns a host entry for the 
user's `.ssh/config`, similar to:
```
Host mytoken-wlcg
  HostName mytoken.data.kit.edu
  Port 2222
  User dj38djn2KD2Kd92jdajkjd
```

We recommend that users store such a Host entry.

## Using the `ssh` Grant
The ssh grant can be used with any ssh client. However, more complex requests that include request data might be 
rather complex to write and it is recommended to use the mytoken client instead.

!!! client "Command Line Client"
    On any action that can also be done via `ssh`, the `--ssh` option can be used with the command line client to 
    use the ssh grant type. The easiest way to use this option is to pass the name of a stored host entry, but it is 
    also possible to pass more complex ssh parameters. The following are examples of valid `--ssh` parameters:
    
    - `mytoken-wlcg` (The name of a host entry)
    - `"-p 2222 user@host"`
    - `"-p 2222 -i ~/.ssh/custom_key_name user@host"`

But, as stated, it is also possible to use other ssh clients.
In the following we assume that a host entry named `mytoken` is used, but of course it is possible to use other options.

The general syntax for ssh requests is:

    ssh mytoken <action> [<mime_type> <data>]

where:
    - `<action>` is one of the supported actions
    - `<mime_type>` is the mime type of the `<data>`
        - The supported mime types are `application/json` and `application/json;base64`
    - `<data>` is the request's data


### Requesting an Access Token through `ssh`

- Requesting an access token with the default scopes and audiences as defined for the ssh key:
    ```
    ssh mytoken AT
    ```
- Requesting an access token with specific scopes / audiences:
    ```
    ssh mytoken AT 'application/json;base64' eyJzY29wZSI6Im9wZW5pZCBwcm9maWxlIGVtYWlsIn0K
    ssh mytoken AT 'application/json;base64' $(cat /tmp/req | base64)
    ssh mytoken AT 'application/json;base64' $(echo '{"scope":"openid profile email"}' | base64)
    mytoken AT -s openid -s profile -s email --ssh=mytoken
    ```


### Requesting a Mytoken through `ssh`

- Requesting a mytoken with the same capabilities and restrictions as defined for the ssh key:
    ```
    ssh mytoken MT
    ```
- Requesting a mytoken with specific capabilities / restrictions:
    ```
    ssh mytoken MT 'application/json;base64' eyJyZXN0cmljdGlvbnMiOnsiZXhwIjogMTI5Mzg0OTE4MDN9LCAiY2FwYWJpbGl0aWVzIjpbIkFUIiwgInRva2VuaW5mb19pbnRyb3NwZWN0Il19Cg==
    ssh mytoken MT 'application/json;base64' $(cat /tmp/req | base64)
    ssh mytoken MT 'application/json;base64' $(echo '{"restrictions":[{"usages_AT": 1, "usages_other":1}]}' | base64)
    mytoken MT --restrictions='{"usages_AT": 1, "usages_other": 1}' --ssh=mytoken
    ```
  
### Tokeninfo through `ssh`

- `ssh mytoken introspect`
- `ssh mytoken history`
- `ssh mytoken list-subtokens`
- `ssh mytoken list-all-tokens`

