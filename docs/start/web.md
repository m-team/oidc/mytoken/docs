---
title: Web
---

# Mytoken Web Interface

A demo instance of mytoken is running at [https://mytoken.data.kit.edu](https://mytoken.data.kit.edu).

The webinterface can be used to
- create new mytokens
- get information about existing mytokens, including their content, history, and information about subtokens
- exchange a transfer code into a mytoken

If logged in additional functionality is available:
- Obtaining access tokens
- List your mytokens and revoke them
- Change settings

To log in select your OpenID Connect Provider (OP) on the upper right side and authenticate against the OP.
