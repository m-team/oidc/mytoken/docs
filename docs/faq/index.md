---
title: FAQs
---

# Frequently Asked Questions

## About Mytokens and other Concepts
??? mytoken "What is the format of a `mytoken`?"
    By default `mytokens` are represented as JWTs. Refer to [the technical details](../dev/technical/mytoken.md) for 
    information about the encoded data.
    For use-cases where shorter tokens are needed a mytoken can also be represented as a `short mytoken` - a much shorter but opaque string.

??? mytoken "What is the difference between a `mytoken` and an OIDC `refresh token`?"
    Indeed `mytokens` and `refresh token` are related. One can see a `mytoken` as a `proxy` to the `refresh token`. The
    mytoken server uses the `refresh token` in the background and stores it securely; the user receives the linked `mytoken`.

    There are two main differences, between `mytokens` and `refresh tokens`:
    
    - `mytokens` can be passed around while `refresh tokens` cannot
    - `mytokens` can have additional [`restrictions`](../concepts/restrictions.md)

??? question "What is the difference between the `mytoken service` and `oidc-agent`?"
    Both promise to offer access tokens on the command line. The main difference is that `oidc-agent` was designed as a
    local agent. While this has advantages (refresh token never leaves local machine) it also requires each user to have
    an own OIDC client (or to use a public client). Mytoken with a central mytoken server on the other hand can be used
    without registering OIDC clients, and the mytoken tokens can also be easily used on multiple devices - therefore also
    enabling use cases such as usage within long-running compute jobs.
    
    Note that oidc-agent has also support for mytokens, i.e. you can use mytokens with oidc-agent.

## About the Client

??? client "What is the difference between the `mytoken client` and `oidc-agent`?"
    Both can be used to obtain access tokens on the command by using a mytoken. However, the mytoken client does not 
    manage your mytokens and you have to take care of that yourself, i.e. you have to pass in a mytoken.

    `oidc-agent` stores a "master" mytoken in its agent account configuration and uses it to obtain access tokens 
    and also sub-mytokens.

??? client "Where is the config file located?"
    If the directory `~/.config` exists the config file is located inside `~/.config/mytoken` otherwise in `~/.mytoken`.

## About the Server
??? server "Can I run my own `mytoken` server?"
    Of course. See [server deployment](../server/index.md) for the details.

## Getting Involved
??? question "Where can I report bugs?"
    To report a bug or request a feature open an issue at GitHub ([server](https://github.com/oidc-mytoken/server), [client](https://github.com/oidc-mytoken/client)) or write us an email at [m-contact@lists.kit.edu](mailto:m-contact@lists.kit.edu).

??? question "Can I contribute to the project?"
    Sure. We are happy to receive pull requests.