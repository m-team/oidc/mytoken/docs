#!/bin/bash

#pip install mkdocs-bootswatch
#pip install mkdocs-macros-plugin
mkdocs build --site-dir /tmp/mytoken-docs
rsync -rlutopxv --delete --chmod=ug+rw --chown=mytoken:mytoken --numeric-ids \
    /tmp/mytoken-docs/ cvs.data.kit.edu:/var/lighttpd/mytoken-docs.data.kit.edu
