# mytoken

This repo holds the documentation for the mytoken service.

The mytoken repos are at:
- https://github.com/oidc-mytoken/server
- https://github.com/oidc-mytoken/client

A demo instance runs at: https://mytoken.data.kit.edu

Documentation is hosted at:
- https://mytoken-docs.data.kit.edu/
